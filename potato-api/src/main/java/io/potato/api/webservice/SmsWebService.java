/**
 * 
 */
package io.potato.api.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

/**
 * 短信服务WebService接口
 * @author timl
 * created: 2019年1月14日 下午5:38:10
 */
@WebService
@Component
public interface SmsWebService {
	
	/**
	 * 连接服务， 验证用户是否合法
	 * @param username 用户名
	 * @param password 密码
	 * @return  XML结果字符
	 */
	@WebMethod(operationName="ConnMas")
	@WebResult(name="ConnMasReturn",targetNamespace="")
	String ConnMas(
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password);
	
	/**
	 * 批量获取上发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param destaddr 
	 * 目的地址,即移动分配号码所带扩展号码,
	 * 考虑到不同业务系统用同一账号登录调用,
	 * 则不同业务系统建议扩展号码分开,以便区分
	 * @return XML结果字符
	 */
	@WebMethod
	String getUpSms(
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password, 
			@WebParam(name="destaddr") String destaddr);
	
	/**
	 * 获取上行短信确认
	 * @param username 用户名
	 * @param password 密码
	 * @param msgid 短信唯一标识(批量获取上发短信返回的msgid),多个唯一表示用英文逗号(,)隔开,例如: 122,123,124,125
	 * @return XML结果字符
	 */
	@WebMethod(operationName="RspUpSms")
	String RspUpSms (
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password, 
			@WebParam(name="msgid") String msgid);
	
	/**
	 * 插入下发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 接入号的扩展码(不超过两位),各个系统用同一账号情况下请用不同批次号码
	 * @param sendbody 发送主体
	 * @return XML结果字符
	 */
	@WebMethod(operationName="InsertDownSms")
	String InsertDownSms(
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password,
			@WebParam(name="batch")    String batch, 
			@WebParam(name="sendbody") String sendbody);
	
	/**
	 * 下发短信状态报告
	 * @param username 用户名
	 * @param password 密码
	 * @param Batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param cnt 获取的短信状态条数 <=10
	 * @return XML结果字符
	 */
	@WebMethod
	String getDownSmsResult(
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password, 
			@WebParam(name="Batch")    String Batch, 
			@WebParam(name="cnt")      String cnt);
	
	/**
	 * 获取指定下发短信状态
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param msgid 短信标识,多个标识可用英文逗号(,)隔开,例如:1000,1002,1003
	 * @return
	 */
	@WebMethod
	String getSpecialDownSmsResult(
			@WebParam(name="username") String username, 
			@WebParam(name="password") String password,
			@WebParam(name="batch")    String batch, 
			@WebParam(name="msgid")    String msgid);
	
}
