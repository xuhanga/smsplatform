/**
 * 
 */
package io.potato.api.webservice.web;

import io.potato.api.webservice.SmsWebService;
import io.potato.core.util.SpringUtil;
import io.potato.ts.webservice.SmsApiService;

/**
 * 短信服务WebService实现类
 * @author timl
 * created: 2019年1月14日 下午5:56:55
 */
//@Service
//@WebService(serviceName = "Sms", targetNamespace = "http://ums.sz.gov.cn/services/Sms", endpointInterface = "io.potato.api.webservice.SmsWebService")
public class Sms implements SmsWebService {
	
	//@Autowired
	SmsApiService apiService;
	
	public Sms() {
		apiService = SpringUtil.getBean(SmsApiService.class);
	}

	@Override
	public String ConnMas(String username, String password) {
		return this.apiService.checkUser(username, password);
	}

	@Override
	public String getUpSms(String username, String password, String destaddr) {
		return this.apiService.getUpSms(username, password, destaddr);
	}

	@Override
	public String RspUpSms(String username, String password, String msgid) {
		return this.apiService.rspUpSms(username, password, msgid);
	}

	@Override
	public String InsertDownSms(String username, String password, String batch, String sendbody) {
		return this.apiService.insertDownSms(username, password, batch, sendbody);
	}

	@Override
	public String getDownSmsResult(String username, String password, String Batch, String cnt) {
		return this.apiService.getDownSmsResult(username, password, Batch, cnt);
	}

	@Override
	public String getSpecialDownSmsResult(String username, String password, String batch, String msgid) {
		return this.apiService.getSpecialDownSmsResult(username, password, batch, msgid);
	}
}
