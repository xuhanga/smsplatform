package io.potato.api.webservice;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import javax.xml.namespace.QName;

/**
 * 调用接口demo
 * @author timl
 */
public class SmsClient {


    static final String url = "https://szsms.sz.gov.cn:8000/services/Sms";

    static final String username = "AjsDaV0+";
    static final String password = "Al0DO11/UnJXZVBnUz1XYQ==";

    public static void main(String[] args) throws Exception {
        ConnMas();

        String orgCode = "50";  // 发送用户标记 一般两位数字
        String mobileNum = "13800138000,13800138001";  //接收号码, 多个逗号分隔
        String content = "测试内容：1+1<3";  // 短信内容
        Integer needReport = 1;  // 是否需要短信报告  1 需要  其他不需要

        String sendbody = "<sendbody>\n" +
                "\t<message>\n" +
                "\t<orgaddr>%s</orgaddr><mobile>%s</mobile>" +
                "<content><![CDATA[%s]]></content>" +
                "<sendtime></sendtime>" +
                "<needreport>%s</needreport>\n" +
                "\t</message>\n" +
                "\t<publicContent></publicContent>\n" +
                "</sendbody>";
        InsertDownSms(String.format(sendbody, orgCode, mobileNum, content, needReport));
    }

    /**
     * 下发短信
     * @param sendbody
     * @return
     * @throws Exception
     */
    public static String InsertDownSms(String sendbody) throws Exception {
        Service service = new Service();//创建客户端调用webservice的代理对象
        Call call = (Call) service.createCall();//创建一个调用对象，代表对web service 的一次调用
        call.setTargetEndpointAddress(new java.net.URL(url)); //设置web service的url 地址
        call.setOperationName(new QName("", "InsertDownSms")); //设置操作名称，QName 对象的两个参数分别为命名空间和方法名称

        call.addParameter("username", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
        call.addParameter("password", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
        call.addParameter("batch", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);
        call.addParameter("sendbody", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN);

        call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);

        String result = (String) call.invoke(new Object[]{username, password, "34", sendbody});
        System.out.println(result);//打印返回的结果
        return result;
    }

    /**
     * 验证用户名密码
     * @return
     * @throws Exception
     */
    public static String ConnMas() throws Exception {
        Service service = new Service();//创建客户端调用webservice的代理对象
        Call call = (Call) service.createCall();//创建一个调用对象，代表对web service 的一次调用
        call.setTargetEndpointAddress(new java.net.URL(url)); //设置web service的url 地址
        call.setOperationName(new QName("", "ConnMas")); //设置操作名称，QName 对象的两个参数分别为命名空间和方法名称

        call.addParameter("username", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); // 为本次调用的方法增加参数，第一次　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　//参数为参数类型：字符串类型，第二个参数指参数模式：入参
        call.addParameter("password", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN); // 为本次调用的方法增加参数，第一次　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　//参数为参数类型：字符串类型，第二个参数指参数模式：入参
        call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);//为本次调用方法设置返回类型，这里是字符串类型
        String result = (String) call.invoke(new Object[]{username, password});
        System.out.println(result);//打印返回的结果
        return result;
    }

}
