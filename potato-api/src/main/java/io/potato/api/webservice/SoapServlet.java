/**
 * 
 */
package io.potato.api.webservice;

import org.apache.axis.transport.http.AxisServlet;

/**
 * @author timl
 * created: 2019年2月19日 上午10:39:27
 */

@javax.servlet.annotation.WebServlet(
        urlPatterns = {"/ws/*", "/services/*", "/servlet/AxisServlet", "*.jws"} ,
        loadOnStartup = 1,
        name = "AxisServlet"
)
public class SoapServlet  extends AxisServlet {

	private static final long serialVersionUID = 3141688708998192903L;

}
