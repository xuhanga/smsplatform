/**
 * 
 */
package io.potato.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.potato.ts.webservice.SmsApiService;

/**
 * SMS对外接口
 * @author timl
 * created: 2019年1月22日 上午10:26:12
 */
@RestController
public class SmsApiController {
	
	@Autowired
	SmsApiService apiService;
	
	/**
	 *  验证用户是否合法
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 */
	@PostMapping("/sms/checkUser")
	public String checkUser(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password) {
		return this.apiService.checkUser(username, password);
	}
	
	/**
	 * 批量获取上发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param destaddr 
	 * 目的地址,即移动分配号码所带扩展号码,
	 * @return XML结果字符
	 */
	@PostMapping("/sms/getUpSms")
	public String getUpSms(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="destaddr") String destaddr) {
		return this.apiService.getUpSms(username, password, destaddr);
	}
	
	/**
	 * 获取上行短信确认
	 * @param username 用户名
	 * @param password 密码
	 * @param msgid 短信唯一标识,多个用英文逗号(,)隔开,例如: 122,123,124,125
	 * @return XML结果字符
	 */
	@PostMapping("/sms/rspUpSms")
	public String rspUpSms(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="msgid") String msgid) {
		return this.apiService.rspUpSms(username, password, msgid);
	}
	
	/**
	 * 插入下发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 接入号的扩展码(不超过两位),各个系统用同一账号情况下请用不同批次号码
	 * @param sendbody 发送主体
	 * @return XML结果字符
	 */
	@PostMapping("/sms/insertDownSms")
	public String insertDownSms(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="batch") String batch,  
			@RequestParam(name="sendbody") String sendbody) {
		return this.apiService.insertDownSms(username, password, batch, sendbody);
	}
	
	/**
	 * 下发短信状态报告
	 * @param username 用户名
	 * @param password 密码
	 * @param Batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param cnt 获取的短信状态条数 <=10
	 * @return XML结果字符
	 */
	@PostMapping("/sms/getDownSmsResult")
	public String getDownSmsResult(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="Batch") String Batch, 
			@RequestParam(name="cnt")String cnt) {
		return this.apiService.getDownSmsResult(username, password, Batch, cnt);
	}
	
	/**
	 * 获取指定下发短信状态
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param msgid 短信标识,多个标识可用英文逗号(,)隔开,例如:1000,1002,1003
	 * @return
	 */
	@PostMapping("/sms/getSpecialDownSmsResult")
	public String getSpecialDownSmsResult(
			@RequestParam(name="username") String username,
			@RequestParam(name="password") String password,
			@RequestParam(name="batch") String batch, 
			@RequestParam(name="msgid") String msgid) {
		return this.apiService.getSpecialDownSmsResult(username, password, batch, msgid);
	}
}
