/**
 * 
 */
package io.potato.api.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 测试页面
 * @author timl
 *
 */
@RestController()
public class HomeController {
	
	@GetMapping("/")
	public String now() {
		return "potato api works!    " + LocalDateTime.now().toString();
	}

 }
