package io.potato.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 程序入口
 */
@SpringBootApplication
@ComponentScan("io.potato.**")
@ServletComponentScan
@EnableAsync
public class PotatoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PotatoApiApplication.class, args);
    }
}
