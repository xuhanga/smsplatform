package io.potato.test;

import io.potato.task.PotatoTaskApplication;
import io.potato.ts.sms.SmsContent;
import io.potato.ts.sms.SmsSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * author: timl
 * time: 2019-4-4 10:12
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PotatoTaskApplication.class)
public class SmsSenderTest {

    @Autowired
    SmsSender smsSender;

    @Test
    public void testSend() {
        String body = "【新短信平台对接测试，共2条】今日应急专题已完成对接到资源中心新部署的短信平台，本条为测试短信1：按规格宣称，新短信平台可支持的最大短信发送并发量为2000条/秒，发送成功率99%以上。";
        SmsContent smsContent = new SmsContent("13927452867", body);
        smsSender.batchSendDiffSms(Arrays.asList(smsContent),
                "272",
                "",
                null);
    }

}
