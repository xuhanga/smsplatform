/**
 * 
 */
package io.potato.task;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 调度配置, 所有调度都是从这里作为入口
 * 每新增一个调度 都要修改调度线程池大小 
 * @author timl
 * created: 2019年2月15日 上午10:45:26
 */
@Component
@Slf4j
public class Schedule {
	
	@Autowired
	SendSmsTask task;
	
	@Value("${app.job}")
	private String initTask;  //api1,api2,web,
	
	/**
	 * 定时发送短信
	 */
	@PostConstruct
	public void sendDelaySms() {
		try {
			log.warn(initTask);
			if (initTask.contains("api1")) {
				task.runAPITask(0, 9);
			}
			
			if (initTask.contains("api2")) {
				task.runAPITask(10, 19);
			}
			
			if (initTask.contains("web")) {
				task.runWebTask();
			}
			
			if (initTask.contains("resend")) {
				task.runReSendTask();
			}
		} catch (Throwable e) {
			log.error("", e);
		}
	}
	
}
