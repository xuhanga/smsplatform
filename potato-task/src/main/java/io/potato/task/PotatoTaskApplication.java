package io.potato.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 程序入口
 */
@SpringBootApplication
@ComponentScan("io.potato.**")
public class PotatoTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(PotatoTaskApplication.class, args);
    }
}
