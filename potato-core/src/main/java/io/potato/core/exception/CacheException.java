package io.potato.core.exception;

/**
 * 缓存异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年7月1日 上午10:14:28</p>
 */
public class CacheException extends RuntimeException{

	private static final long serialVersionUID = -3844618036577290661L;
	
    public CacheException() {
       super();
    }

    public CacheException(String message) {
        super(message);
    }

    public CacheException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheException(Throwable cause) {
        super(cause);
    }
    
}
