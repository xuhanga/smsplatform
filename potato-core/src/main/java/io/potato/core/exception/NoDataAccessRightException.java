package io.potato.core.exception;

import io.potato.core.AppException;

/**
 * author: timl
 * time: 2019-4-28 10:50
 */
public class NoDataAccessRightException  extends AppException {

    public NoDataAccessRightException() {
        super();
    }

    public NoDataAccessRightException(String message) {
        super(message);
    }

    public NoDataAccessRightException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoDataAccessRightException(Throwable cause) {
        super(cause);
    }

}
