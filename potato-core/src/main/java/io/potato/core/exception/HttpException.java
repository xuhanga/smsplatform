package io.potato.core.exception;

/**
 * HTTP异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:20:23</p>
 */
public class HttpException extends Exception{

	private static final long serialVersionUID = -3844618036577290661L;

    public HttpException() {
        super();
    }

    public HttpException(String message) {
        super(message);
    }

    public HttpException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpException(Throwable cause) {
        super(cause);
    }
}
