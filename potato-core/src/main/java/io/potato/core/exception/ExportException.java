package io.potato.core.exception;

/**
 * 导出异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:20:23</p>
 */
public class ExportException extends RuntimeException{

	private static final long serialVersionUID = -3844618036577290661L;

    public ExportException() {
        super();
    }

    public ExportException(String message) {
        super(message);
    }

    public ExportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExportException(Throwable cause) {
        super(cause);
    }
}
