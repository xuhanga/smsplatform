package io.potato.core.exception;

/**
 * 操作处理异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年7月1日 上午10:14:28</p>
 */
public class OperationHandlerException extends RuntimeException{

	private static final long serialVersionUID = -3844618036577290661L;
	
	private static final String DEFAULT_MESSAGE = "操作失败";

    public OperationHandlerException() {
        super(DEFAULT_MESSAGE);
    }

    public OperationHandlerException(String message) {
        super(message);
    }

    public OperationHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public OperationHandlerException(Throwable cause) {
        super(cause);
    }
    
}
