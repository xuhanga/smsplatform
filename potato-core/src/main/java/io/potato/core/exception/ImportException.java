package io.potato.core.exception;

/**
 * 导入异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:20:23</p>
 */
public class ImportException extends RuntimeException{

	private static final long serialVersionUID = -3844618036577290661L;

    public ImportException() {
        super();
    }

    public ImportException(String message) {
        super(message);
    }

    public ImportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImportException(Throwable cause) {
        super(cause);
    }
    
    /**
     * 构造基于具体单元格的异常
     * 
     * @param row excel行
     * @param cell excel列
     * @param message 错误提示
     * @return 异常对象
     */
    public static ImportException buildCellException(int row, int cell, String message){
    	return new ImportException(String.format("[第%d行第%d列] %s", row, cell, message));
    }
    
    /**
     * 构造基于行的异常
     * 
     * @param row excel行
     * @param message 错误提示
     * @return 异常对象
     */
    public static ImportException buildRowException(int row, String message){
    	return new ImportException(String.format("[第%d行] %s", row, message));
    }
    
}
