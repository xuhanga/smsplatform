package io.potato.core.exception;

/**
 * JSON异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:20:23</p>
 */
public class JsonException extends Exception{

	private static final long serialVersionUID = -3844618036577290661L;

    public JsonException() {
        super();
    }

    public JsonException(String message) {
        super(message);
    }

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonException(Throwable cause) {
        super(cause);
    }
}
