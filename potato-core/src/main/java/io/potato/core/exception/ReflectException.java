package io.potato.core.exception;

/**
 * 反射异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:20:23</p>
 */
public class ReflectException extends RuntimeException {

	private static final long serialVersionUID = -3844618036577290661L;

    public ReflectException() {
        super();
    }

    public ReflectException(String message) {
        super(message);
    }

    public ReflectException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReflectException(Throwable cause) {
        super(cause);
    }
}
