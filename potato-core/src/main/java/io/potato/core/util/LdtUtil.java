/**
 * 
 */
package io.potato.core.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * LocalDatetime  LocalDate 工具类
 * 注意： 时区为东八区
 * @author timl
 * created: 2019年2月23日 上午11:17:38
 */
public class LdtUtil {
	
	public static final DateTimeFormatter defaultDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static final DateTimeFormatter defaultDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public static final DateTimeFormatter defaultDateTime2 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	
	/**
	 * 返回当前毫秒数
	 * @param ldt
	 * @return
	 */
	public static long toMilliSeconds(LocalDateTime ldt) {
		return ldt.toInstant(ZoneOffset.of("+8")).toEpochMilli();
	}
	
	/**
	 * 将java.util.Date 转换为java8 的java.time.LocalDateTime,默认时区为东8区
	 * @param date
	 * @return
	 */
    public static LocalDateTime toLocalDateTime(Date date) {
    	if (date == null) {
    		return null;
    	}
    	return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        // return date.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
    }
 
   
    /**
     * 将java8 的 java.time.LocalDateTime 转换为 java.util.Date，默认时区为东8区
     * @param localDateTime
     * @return
     */
    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.toInstant(ZoneOffset.of("+8")));
    }
    
    /**
     * 日期格式化为yyyy-MM-dd字符
     * @param localDate
     * @return
     */
    public static String format(LocalDate localDate) {
    	return defaultDate.format(localDate);
    }
    
    /**
     * 日期格式化为 yyyy-MM-dd HH:mm:ss 字符
     * @param localDateTime
     * @return
     */
    public static String format(LocalDateTime localDateTime) {
    	return defaultDate.format(localDateTime);
    }
    
    /**
     *  yyyy-MM-dd格式的字符转为日期
     * @param dateStr
     * @return
     */
    public static LocalDate parseLocalDate(String dateStr) {
    	if (StringUtils.isEmpty(dateStr)) {
    		return null;
    	}
    	return LocalDate.parse(dateStr, defaultDate);
    }
    
    /**
     * 字符串转为日期时间， 自动匹配三种格式
     * yyyy-MM-dd，yyyyMMddHHmmss，  yyyy-MM-dd HH:mm:ss
     * @param datetimeStr
     * @return
     */
    public static LocalDateTime parseLocalDateTime(String datetimeStr) {
    	if (StringUtils.isEmpty(datetimeStr)) {
    		return null;
    	}
    	
    	if (datetimeStr.length() == 10) {
    		return LocalDateTime.parse(datetimeStr, defaultDate);
    	} else if (datetimeStr.length() == 14 && datetimeStr.indexOf('-') < 0) {
    		return LocalDateTime.parse(datetimeStr, defaultDateTime2);
    	} else {
    		return LocalDateTime.parse(datetimeStr, defaultDateTime);
    	}
    }
    
    public static void main(String[] args) {
    	System.out.println(LdtUtil.toLocalDateTime(new Date()));
    }
 
}
