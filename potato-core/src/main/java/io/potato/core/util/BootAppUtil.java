package io.potato.core.util;

import java.io.File;

import org.springframework.boot.system.ApplicationHome;

/**
 * 
 * @author timl
 * @time 2019年3月10日下午7:45:00
 * net.yeyou.biz.util.BootAppUtil
 */
public class BootAppUtil {
	/**
	 * 返回指定class所在的目录
	 * @return
	 */
	public static String getCurrentFolder(Class<?> sourceClass) {
		ApplicationHome home = new ApplicationHome(sourceClass);
		File jarFile = home.getSource();
		String folder = jarFile.getParentFile().toString();
		return folder;
	}
}
