/**
 * 
 */
package io.potato.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author timl
 * created: 2018年10月31日 下午6:55:00
 */
public class Lists {
	public static List<String> of(String... strs) {
		List<String> list = new ArrayList<>();
		for (String str : strs) {
			list.add(str);
		}
		return list;
	}

	public static List<Integer> of(Integer... ints) {
		List<Integer> list = new ArrayList<>();
		for (Integer n : ints) {
			list.add(n);
		}
		return list;
	}
	
	public static List<String> toList(String str) {
		List<String> list = new ArrayList<String>();
		String[] arr = str.split(",");
	    for (String s : arr) {
	    	list.add(s.trim());
	    }
	    return list;
	}
}
