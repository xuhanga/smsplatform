package io.potato.core.util;

/**
 * Excel工具类
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午11:52:30</p>
 */
public abstract class ExcelUtils {
	
	/**
	 * Microsoft office excel 2007文件后缀名
	 * 
	 */
	private static final String EXCEL_2007_SUFFIX = ".xlsx";
	
	/**
	 * 判断传入的文件名是否是Microsoft office excel 2007文件,
	 * 判断标准是查看文件后缀是否是.xlsx或者.XLSX，比较简陋，未对文件协议校验
	 * 
	 * @return 是否是Microsoft office excel 2007文件
	 */
	public static boolean isExcel2007(String fileName){
		if(StringUtils.isBlank(fileName)){
			return false;
		}
		
		return fileName.toLowerCase().endsWith(EXCEL_2007_SUFFIX);
	}
	
}
