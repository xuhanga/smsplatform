package io.potato.core.util;

import java.io.Closeable;
import java.io.IOException;

/**
 * IO工具类
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年9月1日 上午10:21:32</p>
 */
public abstract class IOUtils {

	/**
	 * 关闭io资源
	 * 
	 * @param res 资源对象
	 */
	public static void closeResource(Closeable res){
		if(res != null){
			try {
				res.close();
			} catch (IOException e) {
				//ignore exception
			}
		}
	}
	
	/**
	 * 获取文件扩展名
	 * 
	 * @param fileName 文件名
	 * @return 文件扩展名，如传入xxx.jpg则返回.jpg，如传入普通的不带.的字符串，则返回空字符串
	 */
	public static String getFileExtension(String fileName){
		AssertUtils.isNotBlank(fileName);
		int dotPos = fileName.lastIndexOf(".");
		return dotPos == -1 ? "" : fileName.substring(dotPos);
	}
	
}
