/**
 * 
 */
package io.potato.core.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author timl
 * created: 2018年10月31日 下午6:06:02
 */
public class Maps {
	
	public static Map<String, Object> of(String key, Object val) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, val);
		return map;
	}
	
	public static Map<String, Object> of(String key, Object val,String key2, Object val2) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, val);
		map.put(key2, val2);
		return map;
	}
	
	public static Map<String, Object> of(String key, Object val,String key2, Object val2,String key3, Object val3) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, val);
		map.put(key2, val2);
		map.put(key3, val3);
		return map;
	}
	
	public static Map<String, Object> of(String key, Object val,String key2, Object val2,String key3, Object val3,String key4, Object val4) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, val);
		map.put(key2, val2);
		map.put(key3, val3);
		map.put(key4, val4);
		return map;
	}
	
	public Map<String, Object> of(String key, Object val,String key2, Object val2,String key3, Object val3,String key4, Object val4,String key5, Object val5) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, val);
		map.put(key2, val2);
		map.put(key3, val3);
		map.put(key4, val4);
		map.put(key5, val5);
		return map;
	}
	
}
