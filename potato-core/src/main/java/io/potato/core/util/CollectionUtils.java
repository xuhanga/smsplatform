package io.potato.core.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 容器工具类
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:12:14</p>
 */
public abstract class CollectionUtils {

	/**
	 * 判断List是否为null或空集合
	 */
	public static boolean isNullOrEmpty(Collection<?> list){
		return list == null || list.isEmpty();
	}
	
	/**
	 * 判断Map集合是否为null或空集合
	 */
	public static boolean isNullOrEmpty(Map<?, ?> map){
		return map == null || map.isEmpty();
	}

	/**
	 * 判断数组不为null和empty
	 */
	public static <T> boolean isNullOrEmpty(T[] arr){
		return arr == null || arr.length == 0;
	}
	
	/**
	 * 获取分页集合
	 * 
	 * @param list 原集合
	 * @param page 当前页
	 * @param rows 每页显示行数
	 * @return 当前页集合
	 */
	public static <T> List<T> getPageList(List<T> list, int page, int rows){
		if(isNullOrEmpty(list)){
			return list;
		}
		
		int fromIndex = (page - 1) * rows;
		int toIndex = page * rows > list.size() ? list.size() : page * rows;
		return list.subList(fromIndex, toIndex);
	}
	
}
