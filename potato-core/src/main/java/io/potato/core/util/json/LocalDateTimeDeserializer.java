/**
 * 
 */
package io.potato.core.util.json;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

/**
 * @author timl
 * created: 2018年11月8日 上午11:53:38
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
	
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String source = StringDeserializer.instance.deserialize(jsonParser, deserializationContext);
        source = source.trim();
        if (source.length() == 10) {
    		source += " 00:00:00";
    	}
    	
    	if (source.length() == 16) {
    		source += ":00";
    	}
        return LocalDateTime.parse(source, dateTimeFormatter);
    }
}