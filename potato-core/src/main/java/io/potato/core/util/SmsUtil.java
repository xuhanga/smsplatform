/**
 * 
 */
package io.potato.core.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 工具类
 * @author timl
 * created: 2019年2月13日 下午3:09:26
 */
public class SmsUtil {
	
	/**
	 * 号码格式化为+86开头
	 * @param num 手机号码
	 * @return  +86xxxxxxxx格式的手机号码
	 */
	public static String formatPhoneNumber(String num) {
		if (num.startsWith("+86")) {
			return num;
		} else if (num.startsWith("86")) {
			return "+" + num;
		} else {
			return "+86" + num;
		}
	}
	
	public static String trimPhoneNumber(String num) {
		if (num.startsWith("+86")) {
			return num.substring(3);
		} else if (num.startsWith("86")) {
			return num.substring(2);
		} else {
			return num;
		}
	}
	
	/**
	 * 判断是否为手机号码， 按老系统的做法， 不支持86开头
	 * @param num
	 * @return
	 */
	public static boolean isPhoneNumber(String num) {
		if (StringUtils.isEmpty(num)) {
			return false;
		}
		
		return num.matches("^(13|14|15|17|19|16|18)\\d{9}$");
	}
	
	public static void main(String[] args) {
		/*for (int i = 100; i < 300; i++) {
			System.out.println(i + " = " +i % 30);
		}
		
		StringBuilder sb = new StringBuilder();
		for (long i = 17912350001L; i < 17912351000L; i++ ) {
			sb.append(i).append(",");
		}
		
		System.out.println(sb.toString());*/
		
		for (int i = 1; i <= 12; i++) {
			for (int j = 0; j < 10; j++) {
				String m = (i < 10 ? "0":"") + i;
				String table = "xms_sended_record_" + m + j;
				//System.out.println("drop table " + table + ";");
				//System.out.println("create table " + table + " like sms_sended_record;");
				System.out.println("ALTER TABLE `" + table + "` ADD COLUMN `receive_status_desc`  varchar(16) NULL DEFAULT NULL AFTER `receive_status`;");
			}
		}
		
		/*for (int i = 0; i < 20; i++) {
			String table = "xms_sending_record_" + (i%20) ;
			//System.out.println("drop table " + table + ";");
			System.out.println("create table " + table + " like sms_sending_record;");
		}*/
		
	}
	
	/**
	 * 计算长短信会分拆成多少条短信发送出去
	 * @param sign  短信签名
	 * @param content  短信内容
	 * @return  短信条数
	 */
	public static int getSmsCount(String sign, String content) {
		int signLength = sign.length() + 2;
		int cLength = content.length() - 3;
		int smsLength = 67 - signLength;
		
		return (cLength != 0 && cLength % smsLength == 0) 
				? cLength / smsLength : cLength / smsLength + 1 ;
	}
	
	public static boolean isWebUser(String extendCode) {
		return extendCode.startsWith("00") || extendCode.length() > 3;
	}
	
	public static boolean isApiUser(String extendCode) {
		return !isWebUser(extendCode);
	}
	
	public static String getUserCode(String extendCode) {
		if (isWebUser(extendCode)) {
			return extendCode;
		} else {
			return extendCode.substring(0, 3);
		}
	}
	
}
