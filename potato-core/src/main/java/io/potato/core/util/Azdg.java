package io.potato.core.util;

import java.io.IOException;
import java.security.MessageDigest;

/**
 * Azerbaijan Development Group（AzDG）开发的可逆加密算法 AzDGCrypt 对用户资料进行加密。如提供正确的私有密匙，可通过本加密算法对数据进行加密及解密
 * created: 2019年1月15日 上午10:57:18
 */
public class Azdg {
	
	private static final String key = "chinagdn";
	
	private static final Azdg INSTANCE = new Azdg();
	
	/**
	 * 解密
	 * @param cipherText 要解密的内容
	 * @return 解密后的字符串
	 */
	public static String decrypt(String cipherText) {
		return INSTANCE.decrypt(cipherText, key);
	}
	
	/**
	 * 加密
	 * @param txt 要加密的内容
	 * @return 加密后的字符串
	 */
	public static String encrypt(String txt) {
		return INSTANCE.encrypt(txt, key);
	}

	/**
	 * 加密
	 * @param txt
	 * @param key
	 * @return
	 */
	public String encrypt(String txt, String key) {

		String encrypt_key = "0f9cfb7a9acced8a4167ea8006ccd098";
		int ctr = 0;
		String tmp = "";
		int i;

		for (i = 0; i < txt.length(); i++) {
			ctr = (ctr == encrypt_key.length()) ? 0 : ctr;
			tmp = tmp + encrypt_key.charAt(ctr) + (char) (txt.charAt(i) ^ encrypt_key.charAt(ctr));
			ctr++;
		}

		return base64_encode(key(tmp, key));

	}

	/**
	 * 解密
	 * @param cipherText
	 * @param key
	 * @return
	 */
	public String decrypt(String cipherText, String key) {

		// base64解码
		cipherText = base64_decode(cipherText);
		cipherText = key(cipherText, key);
		String tmp = "";

		for (int i = 0; i < cipherText.length(); i++) {
			int c = cipherText.charAt(i) ^ cipherText.charAt(i + 1);
			String x = "" + (char) c;
			tmp += x;
			i++;
		}
		return tmp;
	}

	public String base64_decode(String str) {
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		if (str == null)
			return null;
		try {
			return new String(decoder.decodeBuffer(str));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String key(String txt, String encrypt_key) {

		encrypt_key = strMD5(encrypt_key);
		int ctr = 0;
		String tmp = "";

		for (int i = 0; i < txt.length(); i++) {

			ctr = (ctr == encrypt_key.length()) ? 0 : ctr;
			int c = txt.charAt(i) ^ encrypt_key.charAt(ctr);
			String x = "" + (char) c;
			tmp = tmp + x;
			ctr++;

		}

		return tmp;

	}

	public String base64_encode(String str) {
		return new sun.misc.BASE64Encoder().encode(str.getBytes());
	}

	public static final String strMD5(String s) {

		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };

		try {

			byte[] strTemp = s.getBytes();
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;

			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];

			}

			return new String(str);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
	
	public static void main(String[] args) {
		/*System.out.println(Azdg.encrypt("101"));
		System.out.println(Azdg.encrypt("user_01"));
		System.out.println(Azdg.encrypt("123456"));
		
		System.out.println(Azdg.encrypt("newtest"));
		System.out.println(Azdg.encrypt("272"));
		System.out.println(Azdg.encrypt("Test1234"));*/
		
		System.out.println(Azdg.encrypt("501"));
		System.out.println(Azdg.encrypt("zwsms2019"));
		
		/*for (int i = 272; i < 292; i++) {
	    	 System.out.println(i + " : " + Azdg.encrypt("" + i));
	     }*/
	}


}
