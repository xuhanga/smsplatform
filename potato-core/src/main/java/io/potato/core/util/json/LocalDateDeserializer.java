/**
 * 
 */
package io.potato.core.util.json;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

/**
 * @author timl
 * created: 2018年11月8日 上午11:54:16
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String result = StringDeserializer.instance.deserialize(jsonParser, deserializationContext);
        return LocalDate.parse(result);
    }
}