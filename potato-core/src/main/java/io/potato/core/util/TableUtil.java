/**
 * 
 */
package io.potato.core.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author timl
 * created: 2019年2月13日 上午10:24:49
 */
public class TableUtil {
	
	private static final DateTimeFormatter MONTH_FORMAT = DateTimeFormatter.ofPattern("MM");
	
	private static final String TABLE_NAME_SENDING_REC = "sms_sending_record";
	
	public static String getSendedRecordTable(String userCode, LocalDateTime datetime) {
		if (SmsUtil.isWebUser(userCode)) {
			return "sms_sended_record";
		} else {
			int code = NumberUtils.toInt(userCode, 0);
			return "xms_sended_record_" + datetime.format(MONTH_FORMAT) + (code % 10);
		}
	}
	
	public static String getSendingRecordTable(String userCode) {
		if (SmsUtil.isWebUser(userCode)) {
			return TABLE_NAME_SENDING_REC;
		} else {
			int code = NumberUtils.toInt(userCode, 0);
			return "xms_sending_record_" +  + (code % 20);
		}
	}
	
	public static String getSendingRecordTable(int senderType, int seq) {
		if (senderType == 1) {
			return "xms_sending_record_" + seq;
		} else {
			return TABLE_NAME_SENDING_REC;
		}
		
	}
	
	public static void main(String[] args) {
		System.out.println(TableUtil.getSendedRecordTable("0", LocalDateTime.now()));
	}
	
}
