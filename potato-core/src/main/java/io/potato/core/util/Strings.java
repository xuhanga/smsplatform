/**
 * 
 */
package io.potato.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Objects;
import java.util.UUID;

/**
 * 字符串工具类
 * @author timl
 * created: 2018年10月31日 下午7:58:24
 */
public class Strings {
	
	public static final String EMPTY = "";
	
	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	public static String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return str;
	}
	
	public static String urlFormat(String str) {
		// from=csms19010898123&to=13302910607&body=今天晚上开会【深圳政务短信平台】&extend=1:123&statusCallback=
		StringBuilder buffer = new StringBuilder();
		String[] vars = str.split("&");
		for (String var : vars) {
			String[] pair = var.split("=");
			buffer.append("&").append(pair[0]).append("=").append(pair.length > 1 ? urlEncode(pair[1]) : "");
		}
		buffer.deleteCharAt(0);
		return buffer.toString();
	}
	
	public static String join(int[] arr, final char separator) {
		StringBuilder sb = new StringBuilder();
		for (int i : arr) {
			sb.append(separator).append(i);
		}
		sb.deleteCharAt(0);
		return sb.toString();
	}
	
    public static String join(final Iterable<?> iterable, final char separator) {
        if (iterable == null) {
            return null;
        }
        return join(iterable.iterator(), separator);
    }

    public static String join(final Iterator<?> iterator, final char separator) {

        // handle null, zero and one elements before building a buffer
        if (iterator == null) {
            return null;
        }
        if (!iterator.hasNext()) {
            return EMPTY;
        }
        final Object first = iterator.next();
        if (!iterator.hasNext()) {
            return Objects.toString(first, EMPTY);
        }

        // two or more elements
        final StringBuilder buf = new StringBuilder(256); // Java default is 16, probably too small
        if (first != null) {
            buf.append(first);
        }

        while (iterator.hasNext()) {
            buf.append(separator);
            final Object obj = iterator.next();
            if (obj != null) {
                buf.append(obj);
            }
        }

        return buf.toString();
    }
    
    public static String trimToEmpty(String str) {
    	if (str == null) {
    		return "";
    	}
    	return str.trim();
    }
    
    /**
	 * 判断字符串是否为空字符串
	 * 
	 * 注意：如果字符串全由空格组成，那么视为不能空，返回false
	 * @param str 需要检测的目标字符串
	 * @return
	 */
	public static boolean isEmpty(String str){
		return str == null || str.isEmpty();
	}
	
	/**
	 * 判断字符串是否为空字符串
	 * 
	 * 注意：如果字符串全由空格组成，那么视为空，返回true
	 * @param str 需要检测的目标字符串
	 */
	public static boolean isBlank(String str){
		return str == null || str.trim().isEmpty();
	}
	
	public static void main(String[] args) {
		System.out.println(urlFormat("from=csms19010898123&to=13302910607&body=今天晚上开会【深圳政务短信平台】&extend=1:123&statusCallback="));
	}
	
}
