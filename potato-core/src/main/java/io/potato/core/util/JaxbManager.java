/**
 * 
 */
package io.potato.core.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * jaxb 缓存类
 * @author timl
 * created: 2019年1月14日 上午9:54:25
 */
public final class JaxbManager {
	
	private static final JaxbManager instance = new JaxbManager();
	
    private final ConcurrentMap<String, JAXBContext> contextCache = new ConcurrentHashMap<String, JAXBContext>();
    
    private JaxbManager() {
    }
    
    public static JaxbManager instance() {
        return instance;
    }
    
    public JAXBContext getContext(Class<?> clazz) throws JAXBException {
        JAXBContext context = contextCache.get(clazz.getName());
        if (context == null) {
            context = JAXBContext.newInstance(clazz);
            contextCache.putIfAbsent(clazz.getName(), context);
        }
        return context;
    }
}
