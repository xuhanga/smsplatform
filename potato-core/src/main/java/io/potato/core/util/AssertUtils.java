package io.potato.core.util;

import java.util.Collection;
import java.util.Map;

/**
 * 断言工具类，如果不符合相应的验证标准，将会抛出相应的运行时异常
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:10:59</p>
 */
public abstract class AssertUtils {
	
	/**
	 * 断言一个对象不为null，如果为null将抛出空指针异常
	 * 
	 * @param obj 需要断言的对象
	 * @throws NullPointerException
	 */
	public static void isNotNull(Object obj){
		isNotNull(obj, "the object argument can not be null.");
	}
	
	/**
	 * 断言一个对象不为null，如果为null将抛出NullPointerException异常
	 * @param obj 需要断言的对象
	 * @param msg 断言失败的异常提示信息
	 * @throws NullPointerException
	 */
	public static void isNotNull(Object obj, String msg){
		if(obj == null){
			throw new NullPointerException("[Assertion failed] - " + msg);
		}
	}
	
	/**
	 * 断言一个字符串不为null和空串，断言失败将抛出IllegalArgumentException异常
	 * @param str 需要断言的字符串
	 * @throws IllegalArgumentException
	 */
	public static void isNotBlank(String str){
		isNotBlank(str, "the object argument can not be null or empty.");
	}
	
	/**
	 * 断言一个字符串不为null和空串，断言失败将抛出IllegalArgumentException异常
	 * @param str 需要断言的字符串
	 * @param msg 断言失败的异常提示信息
	 * @throws IllegalArgumentException
	 */
	public static void isNotBlank(String str, String msg){
		if(Strings.isBlank(str)){
			throw new IllegalArgumentException("[Assertion failed] - " + msg);
		}
	}
	
	/**
	 * 断言一个数组不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param arr 需要断言的数组
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(T[] arr){
		isNotNullOrEmpty(arr, "the object argument can not be null or empty.");
	}
	
	/**
	 * 断言一个数组不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param arr 需要断言的数组
	 * @param msg 断言失败的异常提示信息
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(T[] arr, String msg){
		if(arr == null || arr.length == 0){
			throw new IllegalArgumentException("[Assertion failed] - " + msg);
		}
	}
	
	/**
	 * 断言一个集合不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param list 需要断言的集合
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(Collection<?> list){
		isNotNullOrEmpty(list, "the object argument can not be null or empty.");
	}
	
	/**
	 * 断言一个集合不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param list 需要断言的集合
	 * @param msg 断言失败的异常提示信息 
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(Collection<?> list, String msg){
		if(CollectionUtils.isNullOrEmpty(list)){
			throw new IllegalArgumentException("[Assertion failed] - " + msg);
		}
	}
	
	/**
	 * 断言一个Map不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param map 需要断言的Map
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(Map<?, ?> map){
		isNotNullOrEmpty(map, "the object argument can not be null or empty.");
	}
	
	/**
	 * 断言一个Map不为null和empty，断言失败将抛出IllegalArgumentException异常
	 * 
	 * @param map 需要断言的Map
	 * @param msg 断言失败的异常提示信息 
	 * @throws IllegalArgumentException
	 */
	public static <T> void isNotNullOrEmpty(Map<?, ?> map, String msg){
		if(CollectionUtils.isNullOrEmpty(map)){
			throw new IllegalArgumentException("[Assertion failed] - " + msg);
		}
	}
}
