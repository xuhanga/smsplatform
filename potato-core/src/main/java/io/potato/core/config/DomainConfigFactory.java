package io.potato.core.config;

import org.springframework.beans.factory.DisposableBean;

/**
 * Domain配置工厂
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年8月1日 上午10:49:30</p>
 */
public interface DomainConfigFactory<T> extends DisposableBean {

	/**
	 * 创建配置
	 * 
	 * @param domainClass Domain Class对象 
	 * @return 配置对象
	 */
	T create(Class<?> domainClass);
	
}
