package io.potato.core.config;

import io.potato.core.cache.Cache;
import io.potato.core.cache.MemoryCache;

import java.io.Serializable;

/**
 * 内存缓存Domain配置工厂
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年8月1日 上午11:49:57</p>
 */
public abstract class MemoryDomainConfigFactory<T extends Serializable> extends CacheableDomainConfigFactory<T> {
	
	@Override
	protected Cache<Class<?>, T> getCache() {
		return new MemoryCache<Class<?>, T>();
	}
	
}
