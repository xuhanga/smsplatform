package io.potato.core.cache;

import io.potato.core.exception.CacheException;

/**
 * 缓存接口
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年8月1日 下午12:34:50</p>
 */
public interface Cache<K, V> {

	/**
	 * 获取缓存
	 * 
	 * @param key 缓存key
	 * @return 缓存值
	 * @throws CacheException
	 */
    public V get(K key) throws CacheException;

    /**
     * 设置缓存对象
     * 
     * @param key 缓存key
     * @param value 缓存值
     * @return 缓存值
     * @throws CacheException
     */
    public V put(K key, V value) throws CacheException;

    /**
     * 移除缓存
     * 
     * @param key 缓存key
     * @return 缓存值
     * @throws CacheException
     */
    public V remove(K key) throws CacheException;

    /**
     * 清空所有缓存
     * 
     * @throws CacheException
     */
    public void clear() throws CacheException;

}
