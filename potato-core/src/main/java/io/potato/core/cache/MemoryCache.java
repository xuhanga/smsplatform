package io.potato.core.cache;

import io.potato.core.exception.CacheException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 内存缓存
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年8月1日 下午12:40:14</p>
 */
public class MemoryCache<K, V> implements Cache<K, V> {
	
	private Map<K, V> cache = new ConcurrentHashMap<K, V>();

	@Override
	public V get(K key) throws CacheException {
		return cache.get(key);
	}

	@Override
	public V put(K key, V value) throws CacheException {
		return cache.put(key, value);
	}

	@Override
	public V remove(K key) throws CacheException {
		return cache.remove(key);
	}

	@Override
	public void clear() throws CacheException {
		cache.clear();
	}

}
