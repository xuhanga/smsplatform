package io.potato.core;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author timl
 * created: 2018年11月5日 下午4:25:42
 * @param <T>
 */
public abstract class BulkCrudService<T extends BaseEntity, PK> extends CrudService<T, PK> {
	
	@Autowired
	protected EntityManagerFactory emf;
	
	public List<T> bulkSave(List<T> list) {
		EntityManager entityManager = emf.createEntityManager();
		entityManager.setFlushMode(FlushModeType.COMMIT);
        entityManager.getTransaction().begin();

        list.forEach(item -> entityManager.persist(item));
        
        entityManager.getTransaction().commit();
        entityManager.close();
		return list;
	}
}
