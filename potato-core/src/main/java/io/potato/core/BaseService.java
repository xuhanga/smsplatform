package io.potato.core;

/**
 * 服务类基类
 * author: timl
 * time: 2018/12/11 10:12
 */
public abstract class BaseService {

    /**
     * 检查更新记录数是否正确
     * @param rows   实际更新行数
     * @param expected  期望更新行数
     */
    protected void checkUpdateRows(int rows, int expected) {
        if (rows != expected) {
            throw new AppException("update database error");
        }
    }

    /**
     * 检查更新记录数是否正确
     * @param rows   实际更新行数
     */
    protected void checkUpdateRows(int rows) {
        if (rows <= 0) {
            throw new AppException("update database error");
        }
    }
}
