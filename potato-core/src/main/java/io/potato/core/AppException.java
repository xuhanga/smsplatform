/**
 * 
 */
package io.potato.core;

/**
 * @author timl created: 2018年11月8日 下午4:51:18
 */
public class AppException extends RuntimeException {

	private static final long serialVersionUID = -2190118349243063009L;

	public AppException() {
		super();
	}

	public AppException(String message) {
		super(message);
	}
	
	public AppException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public AppException(Throwable cause) {
        super(cause);
    }

}
