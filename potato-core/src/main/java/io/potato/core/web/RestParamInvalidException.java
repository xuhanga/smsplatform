/**
 * 
 */
package io.potato.core.web;

/**
 * @author timl
 * created: 2018年10月31日 下午5:39:20
 */
public class RestParamInvalidException extends RestException {
	
	private static final long serialVersionUID = 2506207128863225778L;

	public RestParamInvalidException(String paramName) {
		super(6, "parameter is not right: " + paramName);
	}
}
