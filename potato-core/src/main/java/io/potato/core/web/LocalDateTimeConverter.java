/**
 * 
 */
package io.potato.core.web;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 自定义参数转换
 * @author timl
 * created: 2018年11月8日 下午1:51:14
 */
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {
	static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @Override
    public LocalDateTime convert(String source) {
    	source = source.trim();
    	if (source.length() == 10) {
    		source += " 00:00:00";
    	}
    	
    	if (source.length() == 16) {
    		source += ":00";
    	}
       return LocalDateTime.parse(source, dateTimeFormatter); 
    }
}
