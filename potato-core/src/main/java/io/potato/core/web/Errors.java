/**
 * 
 */
package io.potato.core.web;

/**
 * 错误定义
 * @author timl
 *
 */
public class Errors {
	
	public static ErrorMessage TOKEN_ERROR   = new ErrorMessage(1, "token错误，请重新登录");
	public static ErrorMessage TOKEN_TIMEOUT = new ErrorMessage(2, "token已超时");
	
	public static ErrorMessage MISS_PARAMETERS = new ErrorMessage(6, "缺少参数");
	
	public static ErrorMessage USER_NO_REGISTERED = new ErrorMessage(4, "用户还没有注册");
	public static ErrorMessage USER_NOT_ACTIVE = new ErrorMessage(17, "用户未激活");
	public static ErrorMessage USER_NOT_FOUND = new ErrorMessage(16, "用户不存在");
	
	
	public static ErrorMessage INVALID_PASSWORD = new ErrorMessage(9, "密码不正确");
	
	public static ErrorMessage USER_REGISTERED = new ErrorMessage(3, "用户已注册");
	public static ErrorMessage USER_EMAIL_OVERLIMIT = new ErrorMessage(19, "用户发送邮件超出上限");

	public static ErrorMessage MOBILE_IN_USED = new ErrorMessage(20, "手机号码已被其他用户使用");

	public static ErrorMessage HAS_CHECKED_ERROR = new ErrorMessage(15, "今天已经签到过");
	
	
}
