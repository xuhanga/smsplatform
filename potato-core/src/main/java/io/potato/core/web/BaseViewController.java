package io.potato.core.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.HashMap;
import java.util.Map;

/**
 * author: timl
 * time: 2018/12/13 17:24
 */
public abstract class BaseViewController {

    @ModelAttribute
    protected  void initModels(Model model) {
        Map<String, Object> globalMap = new HashMap<String, Object>();
        globalMap.put("username", "admin");
        model.addAttribute("$g", globalMap);
    }

}
