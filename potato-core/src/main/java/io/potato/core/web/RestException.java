/**
 * 
 */
package io.potato.core.web;

/**
 * 
 * @author timl
 * created: 2018年10月31日 下午5:23:53
 */
public class RestException extends Exception {

	private static final long serialVersionUID = -6742863569447133218L;
	
	private Integer code;
	
	public RestException(Integer code, String message) {
		super(message);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
