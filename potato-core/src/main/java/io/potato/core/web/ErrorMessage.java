/**
 * 
 */
package io.potato.core.web;

/**
 * @author timl
 *
 */
public class ErrorMessage {
	/**
	 * 错误代码
	 */
	private int code;
	
	/**
	 * 错误信息
	 */
	private String message;
	
	
	public ErrorMessage(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
