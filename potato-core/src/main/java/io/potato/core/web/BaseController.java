/**
 * 
 */
package io.potato.core.web;

/**
 * @author Administrator
 *
 */
public class BaseController {
	
	/**
	 * 非空字符
	 * @param param  要检查的参数
	 * @param paramName  参数名
	 * @throws RestException
	 */
	protected void isNotEmpty(String param, String paramName) throws RestException {
		if (param == null || param.trim().length() == 0) {
			throw new RestParamInvalidException(paramName);
		}
	}
	
	/**
	 * 不是NULL
	 * @param param  要检查的参数
	 * @param paramName  参数名
	 * @throws RestException
	 */
	protected void isNotNull(Object param, String paramName) throws RestException {
		if (param == null) {
			throw new RestParamInvalidException(paramName);
		}
	}
	
	/**
	 * 大于等于0
	 * @param param  要检查的参数
	 * @param paramName  参数名
	 * @throws RestException
	 */
	protected void isZeroOrPositive(Integer param, String paramName) throws RestException {
		if (param == null || param < 0) {
			throw new RestParamInvalidException(paramName);
		}
	}
	
	/**
	 *     大于0
	 * @param param  要检查的参数
	 * @param paramName  参数名
	 * @throws RestException
	 */
	protected void isPositive(Integer param, String paramName) throws RestException {
		if (param == null || param <= 0) {
			throw new RestParamInvalidException(paramName);
		}
	}
	
	/**
	 *     字符串长度
	 * @param param  要检查的参数
	 * @param paramName  参数名
	 * @param length   字符串长度 大于等于
	 * @throws RestException
	 */
	protected void stringLength(String param, String paramName, int length) throws RestException {
		if (param == null || param.trim().length() < length) {
			throw new RestParamInvalidException(paramName);
		}
	}
	
	
	protected void restError(Integer code, String errorMessage) throws RestException {
		throw new RestException(code, errorMessage);
	}
	
	protected void restError(ErrorMessage error) throws RestException {
		throw new RestException(error.getCode(), error.getMessage());
	}
	
}
