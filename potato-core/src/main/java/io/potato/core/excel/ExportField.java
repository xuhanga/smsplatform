package io.potato.core.excel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * excel导出列注解，用于指定对象的字段和excel的列对应
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年6月22日 上午10:21:01</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, METHOD})
public @interface ExportField {

	/**
	 * 导出序号
	 */
	float order();
	
	/**
	 * 导出显示文本
	 */
	String label();
	
}
