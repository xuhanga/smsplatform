
call mvnw clean package  -Dmaven.test.skip=true

copy /Y  .\potato-api\target\ums-api.jar .\build
copy /Y  .\potato-notify\target\ums-notify.jar .\build
copy /Y  .\potato-task\target\ums-task.jar .\build
copy /Y  .\potato-web\target\ums-web.jar .\build
copy /Y  .\potato-api\target\lib\potato-service-2.0.0-RELEASE.jar .\build\lib
copy /Y  .\potato-api\target\lib\potato-core-2.0.0-RELEASE.jar .\build\lib

pause