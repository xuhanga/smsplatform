package io.potato.notify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 程序入口
 */
@SpringBootApplication
@ComponentScan("io.potato.**")
@EnableAsync
@EnableCaching
public class PotatoNotifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(PotatoNotifyApplication.class, args);
    }
}
