/**
 * 
 */
package io.potato.notify.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 测试页面
 * @author timl
 *
 */
@RestController()
public class HomeController {
	
	@GetMapping("/")
	public String now() {
		return "potato notify works!    " + LocalDateTime.now().toString();
	}

 }
