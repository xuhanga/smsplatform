/*
Navicat MySQL Data Transfer

Source Server         : beirx.text
Source Server Version : 50628
Source Host           : www.beirx.cn:60335
Source Database       : sms_test

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2019-06-25 15:53:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sms_sended_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_sended_record`;
CREATE TABLE `sms_sended_record` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35496673453944833 DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for sms_sended_stat
-- ----------------------------
DROP TABLE IF EXISTS `sms_sended_stat`;
CREATE TABLE `sms_sended_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(8) DEFAULT NULL COMMENT '用户账号（拓展码）',
  `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型： 1 API   2 WEB',
  `organ_id` int(11) DEFAULT NULL,
  `stat_year` int(11) DEFAULT NULL,
  `stat_month` tinyint(4) DEFAULT NULL COMMENT '统计的月份',
  `stat_day` date DEFAULT NULL COMMENT '统计的天',
  `total` int(11) DEFAULT NULL COMMENT '总数',
  `success` int(11) DEFAULT NULL COMMENT '成功数',
  `failed` int(11) DEFAULT NULL COMMENT '失败数',
  `total1` int(11) DEFAULT NULL COMMENT '移动总数',
  `success1` int(11) DEFAULT NULL COMMENT '移动成功数',
  `failed1` int(11) DEFAULT NULL COMMENT '移动失败数',
  `total2` int(11) DEFAULT NULL COMMENT '电信总数',
  `success2` int(11) DEFAULT NULL COMMENT '电信成功数',
  `failed2` int(11) DEFAULT NULL COMMENT '电信失败数',
  `total3` int(11) DEFAULT NULL COMMENT '联通总数',
  `success3` int(11) DEFAULT NULL COMMENT '联通成功数',
  `failed3` int(11) DEFAULT NULL COMMENT '联通失败数',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_code` (`user_code`,`stat_day`) USING BTREE,
  KEY `organ_id` (`organ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for sms_sending_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_sending_record`;
CREATE TABLE `sms_sending_record` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE,
  KEY `try_at` (`try_at`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for sms_status_report
-- ----------------------------
DROP TABLE IF EXISTS `sms_status_report`;
CREATE TABLE `sms_status_report` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `msg_id` varchar(64) DEFAULT NULL COMMENT '发送短信成功时返回的短信唯一标识',
  `hw_msg_id` varchar(64) DEFAULT NULL,
  `total` tinyint(4) DEFAULT NULL COMMENT '长短信拆分后的短信条数',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态值 0 成功， 其他失败',
  `status_desc` varchar(16) DEFAULT NULL COMMENT '短信状态描述值',
  `source` tinyint(4) DEFAULT NULL COMMENT '短信状态报告来源：\r\n1：短信平台自行产生的状态报告。\r\n2：短信网关返回的状态报告。',
  `org_code` varchar(7) DEFAULT NULL COMMENT '透传南向网元返回的状态码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方号码',
  `read_status` tinyint(4) DEFAULT NULL COMMENT '客户端是否已经查询过 1 已查询  0 未查询',
  `need_report` tinyint(4) DEFAULT NULL COMMENT '是否需要状态报告',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `batch_code` (`batch_code`,`status`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COMMENT='短信状态报告';

-- ----------------------------
-- Table structure for sms_up_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_up_record`;
CREATE TABLE `sms_up_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '接收人用户ID',
  `msg_id` varchar(48) DEFAULT NULL COMMENT '上行短信的唯一标识',
  `hw_msg_id` varchar(64) DEFAULT NULL,
  `from_num` varchar(24) DEFAULT NULL COMMENT '上行短信发送方的号码',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '上行短信接收方的号码 完整的号码',
  `content` varchar(255) DEFAULT NULL COMMENT '短信内容',
  `user_code` varchar(7) DEFAULT NULL COMMENT '接收用户的拓展码l如单位拓展码',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '完整的接收方拓展码',
  `read_times` tinyint(4) DEFAULT NULL COMMENT '客户端下载次数',
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '上行短信接收时间',
  `status` tinyint(4) DEFAULT NULL COMMENT '0  正常   1  已废弃',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `extend_code` (`extend_code`,`read_times`) USING BTREE,
  KEY `msg_id` (`msg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COMMENT='上行短信（已收到的短信）';

-- ----------------------------
-- Table structure for t_black_list
-- ----------------------------
DROP TABLE IF EXISTS `t_black_list`;
CREATE TABLE `t_black_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_black_words
-- ----------------------------
DROP TABLE IF EXISTS `t_black_words`;
CREATE TABLE `t_black_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `words` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_code_seg
-- ----------------------------
DROP TABLE IF EXISTS `t_code_seg`;
CREATE TABLE `t_code_seg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL COMMENT '号段 如  139， 189',
  `dealer_id` tinyint(4) NOT NULL COMMENT '运营商 1 中国移动  2 中国电信 3 中国联通',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COMMENT='手机号码号段运营商对应表';

-- ----------------------------
-- Table structure for t_contacts
-- ----------------------------
DROP TABLE IF EXISTS `t_contacts`;
CREATE TABLE `t_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '创建的用户ID',
  `user_code` varchar(7) DEFAULT NULL,
  `organ_id` int(11) DEFAULT NULL COMMENT '所属单位ID',
  `name` varchar(64) DEFAULT NULL COMMENT '联系人姓名',
  `mobile_no` varchar(24) DEFAULT NULL COMMENT '手机号码',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 个人通讯录  2  共享通讯录',
  `gender` tinyint(4) DEFAULT NULL COMMENT '性别  1： 男 2： 女',
  `show_order` int(11) DEFAULT NULL COMMENT '显示顺序',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录插入时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '记录最后修改时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `organ_id` (`organ_id`) USING BTREE,
  KEY `user_code` (`user_code`),
  KEY `mobile_no` (`mobile_no`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8mb4 COMMENT='通讯录--联系人表';

-- ----------------------------
-- Table structure for t_contacts_group
-- ----------------------------
DROP TABLE IF EXISTS `t_contacts_group`;
CREATE TABLE `t_contacts_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL COMMENT '通讯录群组名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级群组名称',
  `user_id` int(11) DEFAULT NULL COMMENT '所属用户ID',
  `organ_id` int(11) DEFAULT NULL COMMENT '单位ID',
  `type` tinyint(4) DEFAULT NULL COMMENT '1 个人通讯录  2  共享通讯录',
  `show_order` int(11) DEFAULT NULL COMMENT '显示顺序',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '记录插入时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '记录最后修改时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `organ_id` (`organ_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='通讯录群组';

-- ----------------------------
-- Table structure for t_contacts_link
-- ----------------------------
DROP TABLE IF EXISTS `t_contacts_link`;
CREATE TABLE `t_contacts_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contracts_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `organ_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COMMENT='通讯录群组和通讯录联系人关系表';

-- ----------------------------
-- Table structure for t_dealer
-- ----------------------------
DROP TABLE IF EXISTS `t_dealer`;
CREATE TABLE `t_dealer` (
  `dealer_id` tinyint(4) NOT NULL,
  `dealer_name` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`dealer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='运营商';

-- ----------------------------
-- Table structure for t_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dept_name` varchar(64) DEFAULT NULL COMMENT '部门名称',
  `show_order` int(11) DEFAULT NULL COMMENT '显示顺序',
  `organ_id` int(11) DEFAULT NULL COMMENT '所属单位ID',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL COMMENT '父菜单ID',
  `name` varchar(64) DEFAULT NULL COMMENT '菜单名',
  `icon` varchar(64) DEFAULT NULL COMMENT '图标',
  `href` varchar(255) DEFAULT NULL COMMENT '地址',
  `permission` varchar(64) DEFAULT NULL COMMENT 'shiro permssion',
  `show_order` int(11) DEFAULT NULL COMMENT '显示顺序',
  `status` tinyint(4) DEFAULT '0' COMMENT '0 正常 1 禁用',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_mobile_file
-- ----------------------------
DROP TABLE IF EXISTS `t_mobile_file`;
CREATE TABLE `t_mobile_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '所属用户ID',
  `name` varchar(32) DEFAULT NULL COMMENT '号码文件别名',
  `file_name` varchar(128) DEFAULT NULL COMMENT '号码文件名称',
  `num_count` int(11) DEFAULT '0' COMMENT '号码个数',
  `nums` mediumtext COMMENT '号码列表， 分隔符是空格',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='号码文件表';

-- ----------------------------
-- Table structure for t_organ
-- ----------------------------
DROP TABLE IF EXISTS `t_organ`;
CREATE TABLE `t_organ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL COMMENT ' 单位名称',
  `user_num` int(11) DEFAULT NULL COMMENT '人数',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinyint(4) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_sms_status
-- ----------------------------
DROP TABLE IF EXISTS `t_sms_status`;
CREATE TABLE `t_sms_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `status_desc` varchar(16) DEFAULT NULL,
  `remark` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(8) NOT NULL COMMENT '账号(用户短信拓展码)',
  `user_name` varchar(128) NOT NULL COMMENT '用户名',
  `real_name` varchar(128) DEFAULT NULL COMMENT '姓名',
  `mobile_no` varchar(24) DEFAULT NULL,
  `organ_id` int(11) DEFAULT NULL COMMENT '单位ID',
  `dept_id` int(11) DEFAULT NULL COMMENT '用户所属的部门',
  `parent_code` varchar(8) DEFAULT NULL,
  `user_type` tinyint(4) DEFAULT NULL COMMENT '用户类型： 1 API   2 WEB',
  `password` varchar(64) DEFAULT NULL,
  `password_salt` varchar(16) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '1 男  2 女',
  `birthday` date DEFAULT NULL,
  `office_phone` varchar(24) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `role` tinyint(4) DEFAULT '1' COMMENT '1 普通用户 2 单位管理员  4 超级管理员',
  `sms_sign` varchar(16) DEFAULT NULL COMMENT '用户短信签名',
  `sms_limit_by` tinyint(4) DEFAULT NULL,
  `sms_limit_num` int(11) DEFAULT NULL,
  `locked` tinyint(4) DEFAULT '0' COMMENT '0 未锁定， 1 锁定',
  `last_login` timestamp NULL DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL,
  `need_modify` tinyint(4) DEFAULT NULL COMMENT '是否需要修改密码 0 不需要  1 需要',
  `last_modify` timestamp NULL DEFAULT NULL COMMENT '最后修改密码时间',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_code` (`user_code`) USING BTREE,
  UNIQUE KEY `user_name` (`user_name`) USING BTREE,
  KEY `mobile_no` (`mobile_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29448 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Table structure for t_user_group
-- ----------------------------
DROP TABLE IF EXISTS `t_user_group`;
CREATE TABLE `t_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(128) DEFAULT NULL COMMENT '用户组名称',
  `organ_id` int(11) DEFAULT NULL COMMENT '所属单位ID',
  `user_id` int(11) DEFAULT NULL COMMENT '创建的用户ID',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `organ_id` (`organ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_user_group_link
-- ----------------------------
DROP TABLE IF EXISTS `t_user_group_link`;
CREATE TABLE `t_user_group_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL COMMENT '用户组ID',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for xms_sended_record_010
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_010`;
CREATE TABLE `xms_sended_record_010` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_011
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_011`;
CREATE TABLE `xms_sended_record_011` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_012
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_012`;
CREATE TABLE `xms_sended_record_012` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_013
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_013`;
CREATE TABLE `xms_sended_record_013` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_014
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_014`;
CREATE TABLE `xms_sended_record_014` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_015
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_015`;
CREATE TABLE `xms_sended_record_015` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_016
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_016`;
CREATE TABLE `xms_sended_record_016` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_017
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_017`;
CREATE TABLE `xms_sended_record_017` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_018
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_018`;
CREATE TABLE `xms_sended_record_018` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_019
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_019`;
CREATE TABLE `xms_sended_record_019` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_020
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_020`;
CREATE TABLE `xms_sended_record_020` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_021
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_021`;
CREATE TABLE `xms_sended_record_021` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_022
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_022`;
CREATE TABLE `xms_sended_record_022` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_023
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_023`;
CREATE TABLE `xms_sended_record_023` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_024
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_024`;
CREATE TABLE `xms_sended_record_024` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_025
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_025`;
CREATE TABLE `xms_sended_record_025` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_026
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_026`;
CREATE TABLE `xms_sended_record_026` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_027
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_027`;
CREATE TABLE `xms_sended_record_027` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_028
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_028`;
CREATE TABLE `xms_sended_record_028` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_029
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_029`;
CREATE TABLE `xms_sended_record_029` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_030
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_030`;
CREATE TABLE `xms_sended_record_030` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_031
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_031`;
CREATE TABLE `xms_sended_record_031` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_032
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_032`;
CREATE TABLE `xms_sended_record_032` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_033
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_033`;
CREATE TABLE `xms_sended_record_033` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_034
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_034`;
CREATE TABLE `xms_sended_record_034` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_035
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_035`;
CREATE TABLE `xms_sended_record_035` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_036
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_036`;
CREATE TABLE `xms_sended_record_036` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_037
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_037`;
CREATE TABLE `xms_sended_record_037` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_038
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_038`;
CREATE TABLE `xms_sended_record_038` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_039
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_039`;
CREATE TABLE `xms_sended_record_039` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_040
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_040`;
CREATE TABLE `xms_sended_record_040` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_041
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_041`;
CREATE TABLE `xms_sended_record_041` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_042
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_042`;
CREATE TABLE `xms_sended_record_042` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_043
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_043`;
CREATE TABLE `xms_sended_record_043` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_044
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_044`;
CREATE TABLE `xms_sended_record_044` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_045
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_045`;
CREATE TABLE `xms_sended_record_045` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_046
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_046`;
CREATE TABLE `xms_sended_record_046` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_047
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_047`;
CREATE TABLE `xms_sended_record_047` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_048
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_048`;
CREATE TABLE `xms_sended_record_048` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_049
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_049`;
CREATE TABLE `xms_sended_record_049` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_050
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_050`;
CREATE TABLE `xms_sended_record_050` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_051
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_051`;
CREATE TABLE `xms_sended_record_051` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_052
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_052`;
CREATE TABLE `xms_sended_record_052` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_053
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_053`;
CREATE TABLE `xms_sended_record_053` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_054
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_054`;
CREATE TABLE `xms_sended_record_054` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_055
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_055`;
CREATE TABLE `xms_sended_record_055` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_056
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_056`;
CREATE TABLE `xms_sended_record_056` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_057
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_057`;
CREATE TABLE `xms_sended_record_057` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_058
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_058`;
CREATE TABLE `xms_sended_record_058` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_059
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_059`;
CREATE TABLE `xms_sended_record_059` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_060
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_060`;
CREATE TABLE `xms_sended_record_060` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_061
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_061`;
CREATE TABLE `xms_sended_record_061` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_062
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_062`;
CREATE TABLE `xms_sended_record_062` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_063
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_063`;
CREATE TABLE `xms_sended_record_063` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_064
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_064`;
CREATE TABLE `xms_sended_record_064` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_065
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_065`;
CREATE TABLE `xms_sended_record_065` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_066
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_066`;
CREATE TABLE `xms_sended_record_066` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_067
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_067`;
CREATE TABLE `xms_sended_record_067` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_068
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_068`;
CREATE TABLE `xms_sended_record_068` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_069
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_069`;
CREATE TABLE `xms_sended_record_069` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_070
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_070`;
CREATE TABLE `xms_sended_record_070` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_071
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_071`;
CREATE TABLE `xms_sended_record_071` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_072
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_072`;
CREATE TABLE `xms_sended_record_072` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_073
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_073`;
CREATE TABLE `xms_sended_record_073` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_074
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_074`;
CREATE TABLE `xms_sended_record_074` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_075
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_075`;
CREATE TABLE `xms_sended_record_075` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_076
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_076`;
CREATE TABLE `xms_sended_record_076` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_077
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_077`;
CREATE TABLE `xms_sended_record_077` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_078
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_078`;
CREATE TABLE `xms_sended_record_078` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_079
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_079`;
CREATE TABLE `xms_sended_record_079` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_080
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_080`;
CREATE TABLE `xms_sended_record_080` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_081
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_081`;
CREATE TABLE `xms_sended_record_081` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_082
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_082`;
CREATE TABLE `xms_sended_record_082` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_083
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_083`;
CREATE TABLE `xms_sended_record_083` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_084
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_084`;
CREATE TABLE `xms_sended_record_084` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_085
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_085`;
CREATE TABLE `xms_sended_record_085` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_086
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_086`;
CREATE TABLE `xms_sended_record_086` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_087
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_087`;
CREATE TABLE `xms_sended_record_087` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_088
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_088`;
CREATE TABLE `xms_sended_record_088` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_089
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_089`;
CREATE TABLE `xms_sended_record_089` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_090
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_090`;
CREATE TABLE `xms_sended_record_090` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_091
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_091`;
CREATE TABLE `xms_sended_record_091` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_092
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_092`;
CREATE TABLE `xms_sended_record_092` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_093
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_093`;
CREATE TABLE `xms_sended_record_093` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_094
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_094`;
CREATE TABLE `xms_sended_record_094` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_095
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_095`;
CREATE TABLE `xms_sended_record_095` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_096
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_096`;
CREATE TABLE `xms_sended_record_096` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_097
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_097`;
CREATE TABLE `xms_sended_record_097` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_098
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_098`;
CREATE TABLE `xms_sended_record_098` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_099
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_099`;
CREATE TABLE `xms_sended_record_099` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_100
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_100`;
CREATE TABLE `xms_sended_record_100` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_101
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_101`;
CREATE TABLE `xms_sended_record_101` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_102
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_102`;
CREATE TABLE `xms_sended_record_102` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_103
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_103`;
CREATE TABLE `xms_sended_record_103` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_104
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_104`;
CREATE TABLE `xms_sended_record_104` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_105
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_105`;
CREATE TABLE `xms_sended_record_105` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_106
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_106`;
CREATE TABLE `xms_sended_record_106` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_107
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_107`;
CREATE TABLE `xms_sended_record_107` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_108
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_108`;
CREATE TABLE `xms_sended_record_108` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_109
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_109`;
CREATE TABLE `xms_sended_record_109` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_110
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_110`;
CREATE TABLE `xms_sended_record_110` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_111
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_111`;
CREATE TABLE `xms_sended_record_111` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_112
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_112`;
CREATE TABLE `xms_sended_record_112` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_113
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_113`;
CREATE TABLE `xms_sended_record_113` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_114
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_114`;
CREATE TABLE `xms_sended_record_114` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_115
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_115`;
CREATE TABLE `xms_sended_record_115` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_116
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_116`;
CREATE TABLE `xms_sended_record_116` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_117
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_117`;
CREATE TABLE `xms_sended_record_117` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_118
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_118`;
CREATE TABLE `xms_sended_record_118` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_119
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_119`;
CREATE TABLE `xms_sended_record_119` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_120
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_120`;
CREATE TABLE `xms_sended_record_120` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_121
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_121`;
CREATE TABLE `xms_sended_record_121` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_122
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_122`;
CREATE TABLE `xms_sended_record_122` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_123
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_123`;
CREATE TABLE `xms_sended_record_123` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_124
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_124`;
CREATE TABLE `xms_sended_record_124` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_125
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_125`;
CREATE TABLE `xms_sended_record_125` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_126
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_126`;
CREATE TABLE `xms_sended_record_126` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_127
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_127`;
CREATE TABLE `xms_sended_record_127` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_128
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_128`;
CREATE TABLE `xms_sended_record_128` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sended_record_129
-- ----------------------------
DROP TABLE IF EXISTS `xms_sended_record_129`;
CREATE TABLE `xms_sended_record_129` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送拓展码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消 2 转发回复短信',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '接收短信号码',
  `send_status` tinyint(4) DEFAULT NULL COMMENT '发送状态， 成功提交短信认为发送成功  0成功  其他失败',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称 定时短信由用户自定义， 非定时系统生成',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `total` tinyint(4) DEFAULT '1' COMMENT '长短信拆分后的短信条数',
  `dealer_id` tinyint(4) DEFAULT NULL COMMENT '接收号码运营商ID',
  `submit_time` timestamp NULL DEFAULT NULL COMMENT '短信提交发送的时间',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送实际时间',
  `send_delay` int(11) DEFAULT NULL COMMENT '发送延迟时间',
  `send_dur` int(11) DEFAULT NULL COMMENT '发送所需时间，单位MS',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信接口返回的msg_id',
  `hw_msg_id` varchar(64) DEFAULT NULL COMMENT '华为接口返回的msgid',
  `receive_status` tinyint(4) DEFAULT '-1' COMMENT '接收状态, 用户接收到短信才算接收成功 -1 等待发送结果， 0 成功， 其他失败',
  `receive_status_desc` varchar(16) DEFAULT NULL,
  `receive_time` timestamp NULL DEFAULT NULL COMMENT '接收时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `dest_num` (`dest_num`) USING BTREE,
  KEY `send_time` (`send_time`) USING BTREE,
  KEY `hw_msg_id` (`hw_msg_id`) USING BTREE,
  KEY `task_name` (`task_name`),
  KEY `user_code` (`user_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='已发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_0
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_0`;
CREATE TABLE `xms_sending_record_0` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_1
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_1`;
CREATE TABLE `xms_sending_record_1` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_10
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_10`;
CREATE TABLE `xms_sending_record_10` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_11
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_11`;
CREATE TABLE `xms_sending_record_11` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_12
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_12`;
CREATE TABLE `xms_sending_record_12` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58503 DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_13
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_13`;
CREATE TABLE `xms_sending_record_13` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_14
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_14`;
CREATE TABLE `xms_sending_record_14` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_15
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_15`;
CREATE TABLE `xms_sending_record_15` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_16
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_16`;
CREATE TABLE `xms_sending_record_16` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_17
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_17`;
CREATE TABLE `xms_sending_record_17` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_18
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_18`;
CREATE TABLE `xms_sending_record_18` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_19
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_19`;
CREATE TABLE `xms_sending_record_19` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_2
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_2`;
CREATE TABLE `xms_sending_record_2` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_3
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_3`;
CREATE TABLE `xms_sending_record_3` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_4
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_4`;
CREATE TABLE `xms_sending_record_4` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_5
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_5`;
CREATE TABLE `xms_sending_record_5` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_6
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_6`;
CREATE TABLE `xms_sending_record_6` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_7
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_7`;
CREATE TABLE `xms_sending_record_7` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_8
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_8`;
CREATE TABLE `xms_sending_record_8` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- Table structure for xms_sending_record_9
-- ----------------------------
DROP TABLE IF EXISTS `xms_sending_record_9`;
CREATE TABLE `xms_sending_record_9` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码',
  `batch_code` varchar(7) DEFAULT NULL COMMENT '用户拓展码+批次号',
  `extend_code` varchar(7) DEFAULT NULL COMMENT '实际发送的拓展码 ',
  `dest_num` varchar(24) DEFAULT NULL COMMENT '短信接收方的号码',
  `sender_type` tinyint(4) DEFAULT NULL COMMENT '发送者类型： 1 API用户  2： web用户',
  `sms_type` tinyint(4) DEFAULT NULL COMMENT '短信类型：0：普通短信 1： 定时短信，可以取消',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 待发送  1 已发送 2 已暂停 -1 已取消 -2 已删除',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称， 用户可以自定义， 没有定义自动生成一个',
  `msg_id` varchar(32) DEFAULT NULL COMMENT '短信ID',
  `content` varchar(1024) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '设置的发送时间',
  `lot` varchar(40) DEFAULT NULL COMMENT '批次号, 同一次发送的批次号相同',
  `need_report` tinyint(4) DEFAULT '0' COMMENT '是否需要状态报告',
  `seq` tinyint(4) DEFAULT NULL COMMENT '序号',
  `try_times` tinyint(4) DEFAULT NULL COMMENT '尝试发送次数',
  `try_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`) USING BTREE,
  KEY `dest_num` (`dest_num`),
  KEY `send_time` (`send_time`,`status`,`try_times`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='等待发送的短信';

-- ----------------------------
-- View structure for v_dept
-- ----------------------------
DROP VIEW IF EXISTS `v_dept`;
CREATE VIEW `v_dept` AS select `t`.`id` AS `id`,`t`.`dept_name` AS `dept_name`,`t`.`show_order` AS `show_order`,`t`.`organ_id` AS `organ_id`,`t`.`created_at` AS `created_at`,`t`.`updated_at` AS `updated_at`,count(`t2`.`id`) AS `user_count` from (`t_dept` `t` left join `t_user` `t2` on((`t`.`id` = `t2`.`dept_id`))) group by `t`.`id` ;

-- ----------------------------
-- View structure for v_user
-- ----------------------------
DROP VIEW IF EXISTS `v_user`;
CREATE VIEW `v_user` AS select `t`.`id` AS `id`,`t`.`user_code` AS `user_code`,`t`.`user_name` AS `user_name`,`t`.`real_name` AS `real_name`,`t`.`mobile_no` AS `mobile_no`,`t`.`organ_id` AS `organ_id`,`t1`.`name` AS `organ_name`,`t`.`dept_id` AS `dept_id`,`t2`.`dept_name` AS `dept_name`,`t`.`role` AS `role`,`t`.`user_type` AS `user_type`,(case when (`t`.`role` = 1) then '单位用户' when (`t`.`role` = 2) then '单位管理员' when (`t`.`role` = 4) then '超级管理员' end) AS `role_name`,`t`.`locked` AS `locked`,`t`.`last_login` AS `last_login` from ((`t_user` `t` left join `t_organ` `t1` on((`t`.`organ_id` = `t1`.`id`))) left join `t_dept` `t2` on((`t`.`dept_id` = `t2`.`id`))) ;
