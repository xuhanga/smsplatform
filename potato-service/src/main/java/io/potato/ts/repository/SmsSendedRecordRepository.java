package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import io.potato.ts.domain.SmsSendedRecord;

/**
 * 已发送的短信(sms_sended_record)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface SmsSendedRecordRepository extends JpaRepository<SmsSendedRecord, Long> {
	
	SmsSendedRecord findByHwMsgId(String hwMsgId);
	
	@Modifying
	@Query(nativeQuery = true, value = "update sms_sended_record set receive_status = :status, receive_time = now()  where hw_msg_id = (:hwMsgId)")
	int updateReceiveStatus(String hwMsgId, Integer status);

}
