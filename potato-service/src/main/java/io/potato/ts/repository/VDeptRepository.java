package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.VDept;

/**
 * 部门表(t_dept)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-11 18:22:11</p>
 */
public interface VDeptRepository extends JpaRepository<VDept, Integer> {

}
