package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.CodeSeg;

/**
 * 手机号码号段运营商对应表(t_code_seg)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface CodeSegRepository extends JpaRepository<CodeSeg, Integer> {

}
