package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.BlackList;

/**
 * 黑名单号码表(t_black_list)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:56</p>
 */
public interface BlackListRepository extends JpaRepository<BlackList, Integer> {

}
