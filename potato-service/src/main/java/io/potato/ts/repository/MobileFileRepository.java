package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.MobileFile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 号码文件表(t_mobile_file)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface MobileFileRepository extends JpaRepository<MobileFile, Integer> {

    @Query("from MobileFile WHERE id IN (:ids) AND userId = :userId")
    List<MobileFile> findByIds(@Param("ids") List<Integer> ids, @Param("userId") Integer userId);

    List<MobileFile> findByUserId(@Param("userId") Integer userId);

    @Modifying
    @Query("DELETE from MobileFile WHERE id IN (:ids) AND userId = :userId")
    void deleteByIds(@Param("ids") Integer[] ids, @Param("userId") Integer userId);

    @Modifying
    @Query("delete from MobileFile where userId = :userId")
    void deleteByUserId(@Param("userId") Integer userId);

}
