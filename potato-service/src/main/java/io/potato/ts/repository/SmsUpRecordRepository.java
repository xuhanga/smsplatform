package io.potato.ts.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import io.potato.ts.domain.SmsUpRecord;

/**
 * 上行短信（已收到的短信）(sms_up_record)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-08 09:47:19</p>
 */
public interface SmsUpRecordRepository extends JpaRepository<SmsUpRecord, Integer> {
	
	/**
	 * 根据拓展码查询未读的上行短信
	 * @param extendCode
	 * @return
	 */
	@Query(value = "SELECT * FROM sms_up_record WHERE extend_code like :extendCode AND read_times < 5 limit 30", nativeQuery = true)
	List<SmsUpRecord> findUnRead(@Param("extendCode") String extendCode);
	
	@Modifying
	@Query(value = "update SmsUpRecord set readTimes = readTimes + 1 where id in (:ids)")
	int updateReadTimes(@Param("ids") List<Integer> ids);
	
	@Modifying
	@Query(value = "update SmsUpRecord set readTimes =  6 where msgId in (:ids)")
	int updateReadStatus(@Param("ids") List<String> ids);
	
}
