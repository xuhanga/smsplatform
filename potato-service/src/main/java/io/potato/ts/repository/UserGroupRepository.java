package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.UserGroup;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 用户组(t_user_group)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:57</p>
 */
public interface UserGroupRepository extends JpaRepository<UserGroup, Integer> {

    List<UserGroup> findByOrganId(Integer organId);

    @Modifying
    @Query("delete from UserGroup where userId = :userId")
    void deleteByUserId(@Param("userId") Integer userId);
}
