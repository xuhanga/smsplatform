package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.UserGroupLink;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 用户-用户组关联表(t_user_group_link)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:57</p>
 */
public interface UserGroupLinkRepository extends JpaRepository<UserGroupLink, Integer> {


    @Modifying
    @Query(value = "delete from UserGroupLink where groupId = :groupId AND userId in (:userIds)")
    void deleteByUserId(@Param("groupId") Integer groupId, @Param("userIds") List<Integer> userIds);

    @Modifying
    @Query(value = "delete from UserGroupLink where groupId = :groupId")
    void deleteByGroupId(@Param("groupId") Integer groupId);

    Integer countByGroupIdAndUserId(@Param("groupId") Integer groupId, @Param("userId") Integer userId);

    @Modifying
    @Query("delete from UserGroupLink where userId = :userId")
    void deleteByUserId(@Param("userId") Integer userId);
}
