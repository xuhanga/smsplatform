package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.Dealer;

/**
 * 运营商(t_dealer)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface DealerRepository extends JpaRepository<Dealer, Integer> {

}
