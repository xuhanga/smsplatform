package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.ViewUser;

/**
 * 用户表(t_user)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface ViewUserRepository extends JpaRepository<ViewUser, Integer> {

}
