package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.Dept;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 部门表(t_dept)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-11 18:22:11</p>
 */
public interface DeptRepository extends JpaRepository<Dept, Integer> {

    List<Dept> findByOrganId(@Param("organId") Integer organId);

    @Modifying
    @Query("DELETE from Dept WHERE id IN (:ids) AND organId = :organId")
    void deleteByIds(@Param("ids") Integer[] ids, @Param("organId") Integer organId);

}
