package io.potato.ts.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import io.potato.ts.domain.SmsStatusReport;

/**
 * 短信状态报告(sms_status_report)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface SmsStatusReportRepository extends JpaRepository<SmsStatusReport, Long> {
	
	/**
	 * 根据拓展码查询指定条数
	 * @param extendCode  拓展码
	 * @param count  返回记录数
	 * @return
	 */
	@Query(value = "select * from sms_status_report where extend_code = :extendCode AND read_status = 0 limit :count", nativeQuery = true)
	List<SmsStatusReport> findByExtendCode(@Param("extendCode") String extendCode, @Param("count")int count);
	
	/**
	 * 根据拓展码+批次号查询指定条数
	 * @param batchCode  拓展码+批次号
	 * @param count  返回记录数
	 * @return
	 */
	@Query(value = "select * from sms_status_report where batch_code = :batchCode AND read_status = 0 limit :count", nativeQuery = true)
	List<SmsStatusReport> findByBatchCode(@Param("batchCode") String batchCode, @Param("count")int count);
	
	/**
	 * 根据MSGID查询
	 * @param batchCode
	 * @param ids
	 * @return
	 */
	@Query(value = "select * from sms_status_report where msg_id in (:ids) AND batch_code = :batchCode AND read_status = 0", nativeQuery = true)
	List<SmsStatusReport> findByMsgIds(@Param("batchCode") String batchCode, @Param("ids") List<String> ids);
	
	@Modifying
	@Query(value = "update SmsStatusReport set readStatus =  1 where id in (:ids)")
	int updateReadStatus(@Param("ids") List<Integer> ids);
	
}
