package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.BlackWords;

/**
 * 禁用词语表(t_black_words)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:57</p>
 */
public interface BlackWordsRepository extends JpaRepository<BlackWords, Integer> {

}
