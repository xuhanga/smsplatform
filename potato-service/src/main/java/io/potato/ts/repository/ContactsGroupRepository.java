package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.ContactsGroup;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 通讯录群组(t_contacts_group)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface ContactsGroupRepository extends JpaRepository<ContactsGroup, Integer> {


    @Query("from ContactsGroup where userId = :userId AND type = 1 ORDER BY showOrder")
    List<ContactsGroup> findByUser(@Param("userId") Integer userId);

    @Query("from ContactsGroup where userId = :userId AND parentId = :parentId AND type = 1 ORDER BY showOrder")
    List<ContactsGroup> findByUser(@Param("userId") Integer userId, @Param("parentId") Integer parentId);

    @Query("from ContactsGroup where organId = :organId AND type = 2 ORDER BY showOrder")
    List<ContactsGroup> findShare(@Param("organId") Integer organId);

    @Query("from ContactsGroup where organId = :organId AND parentId = :parentId AND type = 2 ORDER BY showOrder")
    List<ContactsGroup> findShare(@Param("organId") Integer organId, @Param("parentId") Integer parentId);

    Integer countByParentId(@Param("parentId") Integer parentId);

    @Modifying
    @Query("delete from ContactsGroup where userId = :userId")
    void deleteByUserId(@Param("userId") Integer userId);

}
