package io.potato.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.potato.ts.domain.ContactsLink;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 通讯录群组和通讯录联系人关系表(t_contacts_link)数据访问接口
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
public interface ContactsLinkRepository extends JpaRepository<ContactsLink, Integer> {

    @Modifying
    @Query(value = "delete from ContactsLink where groupId = :groupId AND contractsId in (:contactIds)")
    void deleteByContactsId(@Param("groupId") Integer groupId, @Param("contactIds") List<Integer> contactIds);

    @Modifying
    @Query(value = "delete from ContactsLink where groupId = :groupId")
    void deleteByGroupId(@Param("groupId") Integer groupId);

    Integer countByGroupIdAndContractsId(@Param("groupId") Integer groupId, @Param("contractsId") Integer contractsId);

    @Modifying
    @Query("delete from ContactsLink where userId = :userId")
    void deleteByUserId(@Param("userId") Integer userId);

}
