package io.potato.ts.sms;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmsRequest {
	
	private String from;
	
	private String statusCallback = "";
	
	private String extend;
	
	private List<SmsContent> smsContent;

}
