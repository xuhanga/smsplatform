/**
 * 
 */
package io.potato.ts.sms;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * 华为发送短信API
 * @author timl
 * created: 2019年1月8日 下午2:46:17
 */
public interface HuaweiSmsClient {
	
	/**
	 * 批量发送短信
	 * @param wsse   授权加密串
	 * @param from  短信发送方的号码
	 * @param to  短信接收方的号码，号码格式为：+{国家码}{地区码}{终端号码}，当接收方号码为手机号码时，{地区码}可选
	 * 如果携带多个接收方号码，则以英文逗号分隔。每个号码最大长度为21位，最多允许携带1000个号码。
	 * @param body 发送的短信内容，最大长度为2000字节，国内短信必须在短信内容中携带短信签名
	 * @param extend  扩展参数，在状态报告和话单中会原样返回不允许携带以下字符：“{”，“}”（即大括号）
	 * @param callBackUrl  SP的回调地址，用于接收短信状态报告
	 * @return 每条短信的短信ID，发送状态等
	 */
	@RequestLine("POST /sms/batchSendSms/v1")
	@Headers({"Content-Type: application/x-www-form-urlencoded", "X-WSSE: {wsse}"} )
	@Body("from={from}&to={to}&body={body}&extend={extend}"
	      +"&statusCallback={callBackUrl}"
	      )
	SmsResponse batchSendSms(@Param("wsse") String wsse, 
			@Param("from") String from, 
			@Param("to") String to, 
			@Param("body") String body,
			@Param("extend") String extend, 
			@Param("callBackUrl") String callBackUrl);
	
	
	/**
	 * 批量发送不同的短信
	 * @param wsse  授权加密串
	 * @param json  提交的json参数
	 * @return  每条短信的短信ID，发送状态等
	 */
	@RequestLine("POST /sms/batchSendDiffSms/v1")
	@Headers({"Content-Type: application/json", "X-WSSE: {wsse}"} )
	@Body("{json}")
	SmsResponse batchSendDiffSms(@Param("wsse") String wsse, @Param("json") String json);

}
