/**
 *
 */
package io.potato.ts.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author timl
 * created: 2019年2月26日 下午5:44:52
 */
public class SmsApiStatus {


    static Map<String, Integer> receiveStatusMap = new HashMap<>();

    static Map<String, Integer> sendStatusMap = new HashMap<>();

    private static Map<String, String> statusDescMap1 = new HashMap<>();

    private static Map<String, String> statusDescMap2 = new HashMap<>();

    private static Map<String, String> statusDescMap3 = new HashMap<>();

    private static Map<String, String> statusDescMap = new HashMap<>();


    static {

        initStatucDescMap1();
        initStatucDescMap2();
        initStatucDescMap3();

        // 初始化状态报告的接收状态映射
        receiveStatusMap.put("DELIVRD", 0);    // 用户已成功收到短信。
        receiveStatusMap.put("EXPIRED", 101);  // 短信已超时。
        receiveStatusMap.put("DELETED", 102);  // 短信已删除。
        receiveStatusMap.put("UNDELIV", 103);  // 短信递送失败
        receiveStatusMap.put("ACCEPTD", 104);  // 短信已接收
        receiveStatusMap.put("UNKNOWN", 105);  // 短信状态未知
        receiveStatusMap.put("REJECTD", 106);  // 短信被拒绝
        receiveStatusMap.put("RTE_ERR", 107);  // 平台内部路由错误

        statusDescMap.put("EXPIRED", "短信已超时");
        statusDescMap.put("DELETED", "短信已删除");
        statusDescMap.put("UNDELIV", "短信递送失败");
        statusDescMap.put("ACCEPTD", "短信已接收");
        statusDescMap.put("UNKNOWN", "短信状态未知");
        statusDescMap.put("REJECTD", "短信被拒绝");
        statusDescMap.put("RTE_ERR", "平台内部路由错误");

        // 初始化发送状态映射
        sendStatusMap.put("000000", 0); // 成功
        sendStatusMap.put("E200015", 15); // 待发送短信数量太大
        sendStatusMap.put("E200028", 28); // 模板校验失败
        sendStatusMap.put("E200029", 29); // 模板匹配失败
        sendStatusMap.put("E200030", 30); // 模板未激活
        sendStatusMap.put("E200031", 31); // 协议校验失败
        sendStatusMap.put("E200033", 33); // 模板类型不正确
    }

    public static String getSendStatusDesc(int status) {
        if (status == 0) {
            return "成功";
        } else if (status == 15) {
            return "待发送短信数量太大";
        } else if (status == 28) {
            return "模板校验失败";
        } else if (status == 29) {
            return "模板匹配失败";
        } else if (status == 30) {
            return "模板未激活";
        } else if (status == 31) {
            return "协议校验失败";
        } else if (status == 33) {
            return "模板类型不正确";
        } else {
            return "其他错误";
        }
    }

    public static String getReceiveStatusDesc(int dealerId, String statusCode) {

        if (StringUtils.isBlank(statusCode)) {
            return "短信状态未知";
        }

        if (statusDescMap.containsKey(statusCode)) {
            return statusDescMap.get(statusCode);
        }

        String desc = null;

        if (dealerId == 3) {
            desc = statusDescMap3.get(statusCode);
        } else if (dealerId == 2) {
            desc = statusDescMap2.get(statusCode);
        } else {
            desc = statusDescMap1.get(statusCode);
        }

        return desc == null ? "短信状态未知" : desc;
    }

    /**
     * 返回接收状态码
     *
     * @param statusDesc
     * @return
     */
    public static Integer getReceiveStatus(String statusDesc) {
        if (StringUtils.isEmpty(statusDesc)) {
            return 111;
        }

        if (receiveStatusMap.containsKey(statusDesc)) {
            return receiveStatusMap.get(statusDesc);
        } else {
            return 110; // 网关error
        }
    }

    /**
     * 返回发送状态码
     *
     * @param statusDesc
     * @return
     */
    public static Integer getSendStatus(String statusDesc) {
        if (StringUtils.isEmpty(statusDesc)) {
            return 211;
        }

        if (sendStatusMap.containsKey(statusDesc)) {
            return sendStatusMap.get(statusDesc);
        } else {
            if (statusDesc.length() > 3) {
                return NumberUtils.toInt(statusDesc.substring(3), 199); // 网关error
            } else {
                return 199;
            }
        }
    }

    /**
     * 当前返回状态是否为待发送短信数量太大
     *
     * @param statusDesc 接口返回状态值
     * @return 是否为待发送短信数量太大
     */
    public static boolean isOverflow(String statusDesc) {
        return "E200015".equals(statusDesc);
    }

    /**
     * 初始化联通状态码
     */
    public static void initStatucDescMap3() {
        statusDescMap3.put("1", "非法用户");
        statusDescMap3.put("2", "号码被移动屏蔽");
        statusDescMap3.put("3", "未知原因");
        statusDescMap3.put("4", "电信业务不支持");
        statusDescMap3.put("5", "用户鉴权失败");
        statusDescMap3.put("8", "可能是网关短信中心已满");
        statusDescMap3.put("9", "不存在的被叫号码");
        statusDescMap3.put("10", "无法接通或者关机、号码被移动屏蔽");
        statusDescMap3.put("10", "被移动屏蔽");
        statusDescMap3.put("11", "通信服务未提供,欠费停机");
        statusDescMap3.put("12", "空号、联通号码被移动屏蔽");
        statusDescMap3.put("12", "被移动屏蔽");
        statusDescMap3.put("13", "呼叫受限");
        statusDescMap3.put("17", "未知原因");
        statusDescMap3.put("18", "网关瞬间超过短信中心的流量控制");
        statusDescMap3.put("20", "未知原因");
        statusDescMap3.put("22", "路由错");
        statusDescMap3.put("24", "计费号码无效");
        statusDescMap3.put("25", "网关返回状态报告超时的错误");
        statusDescMap3.put("27", "手机不支持短消息或者用户缺席,关机或不在服务区.");
        statusDescMap3.put("28", "手机接收短消息出现错误");
        statusDescMap3.put("29", "网关无日志，不知道的用户.");
        statusDescMap3.put("41", "HLR错误导致");
        statusDescMap3.put("44", "MSC拒绝");
        statusDescMap3.put("46", "网关短信中心已满");
        statusDescMap3.put("51", "黑名单");
        statusDescMap3.put("53", "呼叫被禁止");
        statusDescMap3.put("54", "网关正在更换号码");
        statusDescMap3.put("55", "非法的SME地址");
        statusDescMap3.put("56", "用户未在短信中心开户");
        statusDescMap3.put("57", "绑定ip地址有误或者接入号扩展号没有写对");
        statusDescMap3.put("59", "未知原因");
        statusDescMap3.put("61", "未知原!");
        statusDescMap3.put("67", "黑名单错误");
        statusDescMap3.put("70", "通道黑名单");
        statusDescMap3.put("72", "路由前转错误");
        statusDescMap3.put("77", "超过条数限制");
        statusDescMap3.put("79", "未知原因");
        statusDescMap3.put("88", "瞬间的提交速度太快");
        statusDescMap3.put("90", "未知原因");
        statusDescMap3.put("92", "未知原因");
        statusDescMap3.put("93", "SP费用不足");
        statusDescMap3.put("98", "未知原因");
        statusDescMap3.put("100", "未知原因");
        statusDescMap3.put("102", "未知原因");
        statusDescMap3.put("103", "未知原因");
        statusDescMap3.put("104", "未知原因");
        statusDescMap3.put("113", "此错误网关忽略不计");
        statusDescMap3.put("255", "停机未知原因");
        statusDescMap3.put("89", " MSC无用户信息");
        statusDescMap3.put("69", "可能是分配的端口号太长了，外网号不让发");
        statusDescMap3.put("23", "空号");
        statusDescMap3.put("86", "通道黑名单");
        statusDescMap3.put("101", "空号");
        statusDescMap3.put("101", "提交报告，流速大");
        statusDescMap3.put("18", "红树联通没加回复TD退订");
        statusDescMap3.put("255", "可能通道号大于或者等于21位");
        statusDescMap3.put("216", "只能发长沙本地");
        statusDescMap3.put("15", "红树联通,不能发营销类短信");
        statusDescMap3.put("61", "状态报告，流量大");
        statusDescMap3.put("14", "短信中心超数");
        statusDescMap3.put("68", "企业代码，业务代码没设置");
        statusDescMap3.put("64", "非法用户");
        statusDescMap3.put("104", "可能空号");
        statusDescMap3.put("105", "可能空号");
        statusDescMap3.put("106", "广西联通");
        statusDescMap3.put("59", "关机");
        statusDescMap3.put("182", "暂停服务");
        statusDescMap3.put("117", "打不通");
        statusDescMap3.put("12", "暂停服务，停机");
        statusDescMap3.put("15", "是手机端终或信号问题");
        statusDescMap3.put("36", "信息传输过程中其他省份故障影响所致");
        statusDescMap3.put("73", "网关繁忙，总流控超限");
        statusDescMap3.put("219", "关机");
        statusDescMap3.put("182", "停机");
        statusDescMap3.put("108", "通道关停");
        statusDescMap3.put("113", "通道被总部关停");
        statusDescMap3.put("110", "拓鑫联通：非发送时间拦截失败");
        statusDescMap3.put("237", "回复了TD屏蔽接入号");
        statusDescMap3.put("50", "手机关机或信号不稳定");
        statusDescMap3.put("104", "用户手机短信问题(用户未开通短信业务功能)");
        statusDescMap3.put("105", "呼叫被禁止  该用户的短消息业务被禁止了");
        statusDescMap3.put("106", "主叫用户没有呼叫权限");
        statusDescMap3.put("219", "停机,接收超时");
        statusDescMap3.put("1", "空号");
        statusDescMap3.put("54", "关机");
        statusDescMap3.put("13", "停机");
        statusDescMap3.put("12", "空号");
        statusDescMap3.put("64", "非法用户");
        statusDescMap3.put("59", "关机");
        statusDescMap3.put("182", "暂停服务");
        statusDescMap3.put("117", "手机保存信息空间已满，需要清理");
        statusDescMap3.put("5", "停机");
        statusDescMap3.put("10", "用户关机或内存满");
        statusDescMap3.put("23", "空号，停机，关机");
        statusDescMap3.put("247", "空号，停机，关机");
        statusDescMap3.put("SUBMIT_FAILD", "玄武提交到运营商网关失败");
    }

    /**
     *初始化电信状态码
     */
    public static void initStatucDescMap2() {
        statusDescMap2.put("1", "系统忙,超MT下发流控");
        statusDescMap2.put("2", "超过最大连接数");
        statusDescMap2.put("10", "消息结构错");
        statusDescMap2.put("11", "命令字错");
        statusDescMap2.put("12", "序列号重复");
        statusDescMap2.put("20", "IP 地址错");
        statusDescMap2.put("21", "认证错,用户名、密码错误");
        statusDescMap2.put("22", "版本太高");
        statusDescMap2.put("30", "非法消息类型（MsgType）");
        statusDescMap2.put("31", "非法优先级（Priority）");
        statusDescMap2.put("32", "非法资费类型（FeeType）");
        statusDescMap2.put("33", "非法资费代码（FeeCode）");
        statusDescMap2.put("34", "非法短消息格式（MsgFormat）");
        statusDescMap2.put("35", "非法时间格式");
        statusDescMap2.put("36", "非法短消息长度（MsgLength）");
        statusDescMap2.put("37", "有效期已过");
        statusDescMap2.put("38", "非法查询类别（QueryType）");
        statusDescMap2.put("39", "路由错误，主叫、被叫或付费号码错误");
        statusDescMap2.put("40", "非法包月费/封顶费（FixedFee）");
        statusDescMap2.put("41", "非法更新类型（UpdateType）");
        statusDescMap2.put("42", "非法路由编号（RouteId）");
        statusDescMap2.put("43", "非法服务代码（ServiceId）");
        statusDescMap2.put("44", "非法有效期（ValidTime）");
        statusDescMap2.put("45", "非法定时发送时间（AtTime）");
        statusDescMap2.put("46", "非法发送用户号码（SrcTermId）");
        statusDescMap2.put("47", "非法接收用户号码（DestTermId）");
        statusDescMap2.put("48", "非法计费用户号码（ChargeTermId）");
        statusDescMap2.put("49", "非法SP 服务代码（SPCode）");
        statusDescMap2.put("56", "非法源网关代码（SrcGatewayID）");
        statusDescMap2.put("57", "非法查询号码（QueryTermID）");
        statusDescMap2.put("58", "没有匹配路由");
        statusDescMap2.put("59", "非法SP 类型（SPType）");
        statusDescMap2.put("60", "非法上一条路由编号（LastRouteID）");
        statusDescMap2.put("61", "非法路由类型（RouteType）");
        statusDescMap2.put("62", "非法目标网关代码（DestGatewayID）");
        statusDescMap2.put("63", "非法目标网关IP（DestGatewayIP）");
        statusDescMap2.put("64", "非法目标网关端口（DestGatewayPort）");
        statusDescMap2.put("65", "非法路由号码段（TermRangeID）");
        statusDescMap2.put("66", "非法终端所属省代码（ProvinceCode）");
        statusDescMap2.put("67", "非法用户类型（UserType）");
        statusDescMap2.put("68", "本节点不支持路由更新");
        statusDescMap2.put("69", "非法SP 企业代码（SPID）");
        statusDescMap2.put("70", "非法SP 接入类型（SPAccessType）");
        statusDescMap2.put("71", "路由信息更新失败");
        statusDescMap2.put("72", "非法时间戳（Time）");
        statusDescMap2.put("73", "非法业务代码（MServiceID）");
        statusDescMap2.put("74", "SP禁止下发时段");
        statusDescMap2.put("75", "SP发送超过日流量");
        statusDescMap2.put("76", "SP账号过有效期");
        statusDescMap2.put("18", "内容没加:“回复TD退订”");
        statusDescMap2.put("55", "加黑");
        statusDescMap2.put("67", "提交,无费");
        statusDescMap2.put("134", "超流速");
        statusDescMap2.put("601", "停机");
        statusDescMap2.put("765", "空号");
        statusDescMap2.put("705", "暂停服务");
        statusDescMap2.put("801", "空号，停机");
        statusDescMap2.put("602", "用户关机");
        statusDescMap2.put("702", "用户关机");
        statusDescMap2.put("039", "停机");
        statusDescMap2.put("701", "用户关机");
        statusDescMap2.put("660", "号码问题");
        statusDescMap2.put("869", "空号");
        statusDescMap2.put("771", "暂停服务");
        statusDescMap2.put("601", "停机");
        statusDescMap2.put("999", "业务网关黑名单");
        statusDescMap2.put("039", "空号");
        statusDescMap2.put("614", "用户终端问题");
        statusDescMap2.put("602", "用户关机");
        statusDescMap2.put("701", "用户停机或者空号");
        statusDescMap2.put("760", "可能用户信号问题或者空号");
        statusDescMap2.put("713", "关机，欠费停机或者接受地区网关路由错误");
        statusDescMap2.put("660", "垃圾短信中心拦截");
        statusDescMap2.put("108", "通道停了");
        statusDescMap2.put("6", "空号；也可能是内容有关键字；非白,黑名单");
        statusDescMap2.put("134", "空号");
        statusDescMap2.put("35", "关键字过滤");
        statusDescMap2.put("634", "要电话本地运营商查");
        statusDescMap2.put("705", "用户欠费暂停服务");
        statusDescMap2.put("640", "空号");
        statusDescMap2.put("801", "空号，停机");
        statusDescMap2.put("006", "流量超时");
        statusDescMap2.put("771", "暂停使用");
        statusDescMap2.put("869", "用户回复上行要求屏蔽短信");
        statusDescMap2.put("006", "号码黑名单");
        statusDescMap2.put("875", "贵州地区号码拦截");
        statusDescMap2.put("163", "黑名单");
        statusDescMap2.put("660", "短期内向用户发送短信过多");
        statusDescMap2.put("613", "关机");
        statusDescMap2.put("721", "无线侧端口连接失败");
        statusDescMap2.put("899", "短信内容带有敏感字");
        statusDescMap2.put("SUBMIT_FAILD", "玄武提交到运营商网关失败");
    }

    /**
     * 初始化移动状态码
     */
    public static void initStatucDescMap1() {
        statusDescMap1.put("MK:0255", "未确定的错误原因");
        statusDescMap1.put("MK:0210", "MS错误");
        statusDescMap1.put("MK:0209", "SIM中没有存储短消息的能力");
        statusDescMap1.put("MK:0208", "SIM中存储短消息的空间满");
        statusDescMap1.put("MK:0196", "短消息实体被禁止使用");
        statusDescMap1.put("MK:0195", "扩展短消息实体地址无效");
        statusDescMap1.put("MK:0194", "SMC系统错误");
        statusDescMap1.put("MK:0193", "没有SMC指定（签约）");
        statusDescMap1.put("MK:0192", "SMC忙");
        statusDescMap1.put("MK:0176", "TPDU未被支持");
        statusDescMap1.put("MK:0175", "未指定的TP-Command错误");
        statusDescMap1.put("MK:0160", "操作不能被执行");
        statusDescMap1.put("MK:0159", "未指定的TP-DCS错误");
        statusDescMap1.put("MK:0145", "短消息类型未被支持");
        statusDescMap1.put("MK:0144", "字母表数据编码方案DCS未被支持");
        statusDescMap1.put("MK:0143", "未指定的TP-PID错误");
        statusDescMap1.put("MK:0130", "不能替换短消息");
        statusDescMap1.put("MK:0129", "短消息类型未被支持");
        statusDescMap1.put("MK:0128", "电信业务设备交互未被支持");
        statusDescMap1.put("MK:0079", "短消息超过主叫方的最大提交数，该短消息下发失败后，被删除");
        statusDescMap1.put("MK:0077", "SMC下发短消息时，接口缓冲区满");
        statusDescMap1.put("MK:0069", "FCC接口不可用");
        statusDescMap1.put("MK:0068", "SMC下发短消息给业务模块后，业务模块超时没有返回应答");
        statusDescMap1.put("MK:0067", "无效接口");
        statusDescMap1.put("MK:0066", "因接口临时错误（已注销或未登录）导致短消息下发失败");
        statusDescMap1.put("MK:0065", "GIW超时无应答");
        statusDescMap1.put("MK:0064", "接口无下发短消息的权限");
        statusDescMap1.put("MK:0063", "目的信令点或信令转接点SCCP无法传送该消息");
        statusDescMap1.put("MK:0062", "MTIServer因为流控下发短消息失败");
        statusDescMap1.put("MK:0061", "MAPServer因为流控下发短消息失败");
        statusDescMap1.put("MK:0058", "SGSN系统错误");
        statusDescMap1.put("MK:0057", "MSC系统错误");
        statusDescMap1.put("MK:0056", "HLR系统错误");
        statusDescMap1.put("MK:0055", "SGSN拒绝");
        statusDescMap1.put("MK:0054", "SGSN（Serving GPRS Support Node）无应答");
        statusDescMap1.put("MK:0053", "GIW模块拒绝");
        statusDescMap1.put("MK:0052", "HLR拒绝");
        statusDescMap1.put("MK:0051", "MSC拒绝");
        statusDescMap1.put("MK:0050", "GIW模块（信令网关）无应答");
        statusDescMap1.put("MK:0049", "HLR无应答");
        statusDescMap1.put("MK:0048", "MSC无应答");
        statusDescMap1.put("MK:0046", "HLR版本协商错误");
        statusDescMap1.put("MK:0045", "MAP协议版本错误");
        statusDescMap1.put("MK:0041", "SMC发置位消息后，HLR无应答");
        statusDescMap1.put("MK:0040", "SMC发路由查询请求后，HLR无应答");
        statusDescMap1.put("MK:0037", "来自HLR的未知错误");
        statusDescMap1.put("MK:0036", "来自MSC的未知错误");
        statusDescMap1.put("MK:0035", "来自MSC的意外数据");
        statusDescMap1.put("MK:0034", "来自HLR的意外数据");
        statusDescMap1.put("MK:0033", "SMC没有取到足够的路由信息");
        statusDescMap1.put("MK:0032", "INFORM_SC消息解码错误");
        statusDescMap1.put("MK:0031", "MSC消息解码错误");
        statusDescMap1.put("MK:0030", "HLR消息解码错误");
        statusDescMap1.put("MK:0025", "过滤业务专用错误码");
        statusDescMap1.put("MK:0024", "用户关机");
        statusDescMap1.put("MK:0023", "用户忙");
        statusDescMap1.put("MK:0022", "非法手机");
        statusDescMap1.put("MK:0021", "未知SC");
        statusDescMap1.put("MK:0020", "不正确SME地址");
        statusDescMap1.put("MK:0019", "MS非SC用户");
        statusDescMap1.put("MK:0018", "SC拥塞");
        statusDescMap1.put("MK:0017", "手机内存满");
        statusDescMap1.put("MK:0016", "MS未装备");
        statusDescMap1.put("MK:0015", "MS（Mobile Station）端错误");
        statusDescMap1.put("MK:0014", "意料外的数据");
        statusDescMap1.put("MK:0013", "短消息中心下发短消息给网络侧时，有必选字段缺失");
        statusDescMap1.put("MK:0011", "消息等待队列满");
        statusDescMap1.put("MK:0010", "SM发送失败");
        statusDescMap1.put("MK:0009", "用户不在服务区MWDSET");
        statusDescMap1.put("MK:0008", "用户不在服务区");
        statusDescMap1.put("MK:0007", "设备不支持");
        statusDescMap1.put("MK:0006", "闭合用户群拒绝");
        statusDescMap1.put("MK:0005", "呼叫被禁止");
        statusDescMap1.put("MK:0004", "电信业务不支持");
        statusDescMap1.put("MK:0003", "非法用户");
        statusDescMap1.put("MK:0002", "未定义用户");
        statusDescMap1.put("MK:0001", "未知用户");
        statusDescMap1.put("MK:0000", "正常");
        statusDescMap1.put("MC:0151", "SMSC没有给本网关回状态报告");
        statusDescMap1.put("MC:0001", "SMSC没有给本网关回状态报告");
        statusDescMap1.put("MB:1083", "反欺诈拒绝");
        statusDescMap1.put("MB:1082", "查询CCM失败");
        statusDescMap1.put("MB:1081", "取SRI路由失败");
        statusDescMap1.put("MB:1080", "接口错误");
        statusDescMap1.put("MB:1079", "SMC拒绝该短消息");
        statusDescMap1.put("MB:1078", "所提交的消息无下发路由");
        statusDescMap1.put("MB:1077", "被叫是注册用户黑名单");
        statusDescMap1.put("MB:1076", "主叫是注册用户黑名单");
        statusDescMap1.put("MB:1075", "对短消息的被叫用户进行虚拟短消息中心鉴权失败");
        statusDescMap1.put("MB:1074", "对短消息的主叫用户进行虚拟短消息中心鉴权失败");
        statusDescMap1.put("MB:1073", "对短消息的被叫号码进行帐号鉴权失败");
        statusDescMap1.put("MB:1072", "对短消息的主叫号码进行帐号鉴权失败");
        statusDescMap1.put("MB:1070", "流控错误，短消息中心拥塞");
        statusDescMap1.put("MB:1069", "接口版本不匹配");
        statusDescMap1.put("MB:1065", "UDH（User Data Header）错误");
        statusDescMap1.put("MB:1064", "message_payload存在时UDL（User Data Length）必需为0，否则错误");
        statusDescMap1.put("MB:1063", "message_payload可选参数的值太长");
        statusDescMap1.put("MB:1062", "某个或者两个ports都非法（长度或值错误）");
        statusDescMap1.put("MB:1061", "端口IE（Application Port Addressing）不能与两个ports共存");
        statusDescMap1.put("MB:1060", "提交的消息携带的三个sars的值不满足约束");
        statusDescMap1.put("MB:1058", "分包消息不能再次分包");
        statusDescMap1.put("MB:1057", "SMSC不支持的DCS或错误的DCS");
        statusDescMap1.put("MB:1056", "无效的数据格式（UD数据内容错误）");
        statusDescMap1.put("MB:1052", "PPS错误，暂未使用");
        statusDescMap1.put("MB:1051", "计费用户不存在");
        statusDescMap1.put("MB:1050", "计费用户为NP_OUT用户");
        statusDescMap1.put("MB:1049", "被叫用户为NP_OUT用户");
        statusDescMap1.put("MB:1048", "主叫用户为NP_OUT用户");
        statusDescMap1.put("MB:1047", "计费用户不支持增值业务");
        statusDescMap1.put("MB:1046", "计费用户金额不足");
        statusDescMap1.put("MB:1045", "计费用户状态不正确");
        statusDescMap1.put("MB:1044", "自定义的找不到路由错误");
        statusDescMap1.put("MB:1043", "用户不存在或无效的用户");
        statusDescMap1.put("MB:1042", "空号，或者该号发送次数太多");
        statusDescMap1.put("MB:1041", "主叫用户提交的短消息数超过此用户的最大提交数");
        statusDescMap1.put("MB:1040", "被叫用户金额不足");
        statusDescMap1.put("MB:1039", "主叫用户金额不足");
        statusDescMap1.put("MB:1038", "被叫用户不支持增值业务");
        statusDescMap1.put("MB:1037", "主叫用户不支持增值业务");
        statusDescMap1.put("MB:1036", "被叫用户状态不正确");
        statusDescMap1.put("MB:1035", "主叫用户状态不正确");
        statusDescMap1.put("MB:1034", "PPS鉴权失败");
        statusDescMap1.put("MB:1026", "License受限错误");
        statusDescMap1.put("MB:1025", "无效的短消息中心");
        statusDescMap1.put("MB:1024", "为此条短消息分配内存或其它资源失败");
        statusDescMap1.put("MB:0255", "不明错误");
        statusDescMap1.put("MB:0254", "下发失败");
        statusDescMap1.put("MB:0196", "无效的可选参数");
        statusDescMap1.put("MB:0195", "必需的可选参数丢失");
        statusDescMap1.put("MB:0194", "可选参数的长度错");
        statusDescMap1.put("MB:0193", "命令字中含有被禁止的可选参数");
        statusDescMap1.put("MB:0192", "PDU报文体中的可选部分出错");
        statusDescMap1.put("MB:0103", "query_sm操作失败");
        statusDescMap1.put("MB:0102", "ESME接收端拒绝消息出错");
        statusDescMap1.put("MB:0101", "接收端永久性错误");
        statusDescMap1.put("MB:0100", "接收端暂时错误");
        statusDescMap1.put("MB:0099", "预定义短消息无效或不存在");
        statusDescMap1.put("MB:0098", "短消息中指定的超时时间无效");
        statusDescMap1.put("MB:0097", "短消息中指定的定时时间无效");
        statusDescMap1.put("MB:0088", "短消息数超过了短消息中心的消息队列的最大限定");
        statusDescMap1.put("MB:0085", "消息序号无效");
        statusDescMap1.put("MB:0084", "replace_if_present_flag字段无效");
        statusDescMap1.put("MB:0083", "System_type字段无效");
        statusDescMap1.put("MB:0081", "无效的目的地址NPI");
        statusDescMap1.put("MB:0080", "无效的目的地址TON");
        statusDescMap1.put("MB:0073", "无效的源地址NPI");
        statusDescMap1.put("MB:0072", "无效的源地址TON");
        statusDescMap1.put("MB:0069", "submit_sm或者submit_multi失败");
        statusDescMap1.put("MB:0068", "无法提交到分配表");
        statusDescMap1.put("MB:0067", "ESM_CLASS的值无效");
        statusDescMap1.put("MB:0066", "无效的替换请求");
        statusDescMap1.put("MB:0064", "无效的目的地址列表");
        statusDescMap1.put("MB:0052", "分配列表名错误");
        statusDescMap1.put("MB:0051", "目标地址个数错误");
        statusDescMap1.put("MB:0020", "短消息的服务类型非法");
        statusDescMap1.put("MB:0019", "短消息队列已满");
        statusDescMap1.put("MB:0018", "Replace短消息失败");
        statusDescMap1.put("MB:0017", "Cancel短消息失败");
        statusDescMap1.put("MB:0015", "系统ID错误");
        statusDescMap1.put("MB:0014", "密码错误");
        statusDescMap1.put("MB:0013", "绑定失败");
        statusDescMap1.put("MB:0012", "短消息ID错误");
        statusDescMap1.put("MB:0011", "短消息的目的地址错误");
        statusDescMap1.put("MB:0010", "短消息的源地址错误");
        statusDescMap1.put("MB:0008", "系统错误");
        statusDescMap1.put("MB:0007", "SMC系统错误");
        statusDescMap1.put("MB:0006", "无效的优先标识");
        statusDescMap1.put("MB:0005", "ESME已经绑定");
        statusDescMap1.put("MB:0004", "命令与bind状态不一致");
        statusDescMap1.put("MB:0003", "Command ID非法");
        statusDescMap1.put("MB:0002", "命令长度错误");
        statusDescMap1.put("MB:0001", "消息长度错误");
        statusDescMap1.put("MB:0000", "成功");
        statusDescMap1.put("MA:0054", "超时未接收到响应消息");
        statusDescMap1.put("MA:0053", "发送消息失败");
        statusDescMap1.put("MA:0052", "尚未成功登录");
        statusDescMap1.put("MA:0051", "尚未建立连接");
        statusDescMap1.put("ID:6153", "发送无应答失败");
        statusDescMap1.put("ID:6152", "发送失败");
        statusDescMap1.put("ID:6151", "等待应答过期");
        statusDescMap1.put("ID:6150", "在发送队列中过期");
        statusDescMap1.put("ID:1251", "SMWC 校验失败");
        statusDescMap1.put("ID:1250", "SMWC 校验失败");
        statusDescMap1.put("ID:1249", "SMWC 校验失败");
        statusDescMap1.put("ID:1248", "SMWC 校验失败");
        statusDescMap1.put("ID:1247", "SMWC 校验失败");
        statusDescMap1.put("ID:1246", "SMWC 校验失败");
        statusDescMap1.put("ID:1245", "SMWC 校验失败");
        statusDescMap1.put("ID:1244", "SMWC 校验失败");
        statusDescMap1.put("ID:1243", "SMWC 校验失败 ");
        statusDescMap1.put("ID:1242", "SMMC校验失败");
        statusDescMap1.put("ID:1241", "SMMC校验失败");
        statusDescMap1.put("ID:1240", "SMMC校验失败");
        statusDescMap1.put("ID:0318", "关键字过滤失败");
        statusDescMap1.put("ID:0317", "操作/验证失败");
        statusDescMap1.put("ID:0315", "费率设置错");
        statusDescMap1.put("ID:0314", "实名替换错");
        statusDescMap1.put("ID:0313", "业务验证错");
        statusDescMap1.put("ID:0312", "优先级设置错");
        statusDescMap1.put("ID:0311", "流量控制错");
        statusDescMap1.put("ID:0310", "消息过期");
        statusDescMap1.put("ID:0143", "超过月最大发送MT数量");
        statusDescMap1.put("ID:0142", "超过日最大发送MT数量");
        statusDescMap1.put("ID:0141", "用户处在黑名单中");
        statusDescMap1.put("ID:0140", "用户不在白名单中");
        statusDescMap1.put("ID:0139", "下发时间段违法");
        statusDescMap1.put("ID:0138", "用户相关信息不存在");
        statusDescMap1.put("ID:0137", "伪码信息错误");
        statusDescMap1.put("ID:0136", "用户密码错误");
        statusDescMap1.put("ID:0135", "业务数据同步出错");
        statusDescMap1.put("ID:0134", "EC/SI数据同步出错");
        statusDescMap1.put("ID:0133", "用户数据同步出错");
        statusDescMap1.put("ID:0132", "相关信息不存在");
        statusDescMap1.put("ID:0131", "BOSS系统数据同步出错");
        statusDescMap1.put("ID:0129", "用户已经是梦网用户");
        statusDescMap1.put("ID:0128", "补款,冲正失败");
        statusDescMap1.put("ID:0127", "该用户没有足够的余额");
        statusDescMap1.put("ID:0126", "该用户不是神州行用户");
        statusDescMap1.put("ID:0125", "业务价格超出范围");
        statusDescMap1.put("ID:0124", "业务价格格式错误");
        statusDescMap1.put("ID:0123", "业务价格为负");
        statusDescMap1.put("ID:0122", "接收异常");
        statusDescMap1.put("ID:0121", "没有该类业务");
        statusDescMap1.put("ID:0120", "话单格式错误");
        statusDescMap1.put("ID:0119", "用户不能取消该业务");
        statusDescMap1.put("ID:0118", "用户已经签约了该业务");
        statusDescMap1.put("ID:0117", "该业务不能对该用户开放");
        statusDescMap1.put("ID:0116", "用户暂停签约该业务");
        statusDescMap1.put("ID:0115", "用户没有签约该业务");
        statusDescMap1.put("ID:0114", "EC/SI暂停服务");
        statusDescMap1.put("ID:0113", "EC/SI不存在");
        statusDescMap1.put("ID:0112", "EC/SI代码错误");
        statusDescMap1.put("ID:0111", "该业务尚未开通");
        statusDescMap1.put("ID:0111", "增加企业实名签名，消息内容超长");
        statusDescMap1.put("ID:0110", "该服务种类尚未开通");
        statusDescMap1.put("ID:0109", "该服务种类不存在");
        statusDescMap1.put("ID:0108", "该业务暂停服务");
        statusDescMap1.put("ID:0107", "业务不存在");
        statusDescMap1.put("ID:0106", "服务代码错误");
        statusDescMap1.put("ID:0105", "业务代码错误");
        statusDescMap1.put("ID:0104", "用户没有使用该业务的权限");
        statusDescMap1.put("ID:0103", "用户欠费");
        statusDescMap1.put("ID:0102", "用户停机");
        statusDescMap1.put("ID:0101", "手机号码错误");
        statusDescMap1.put("ID:0100", "手机号码不存在");
        statusDescMap1.put("ID:0097", "此用户为接收者黑名单用户");
        statusDescMap1.put("ID:0096", "此用户为发送者黑名单用户");
        statusDescMap1.put("ID:0089", "到MDSP鉴权时，网关构造等待应答实体失败");
        statusDescMap1.put("ID:0088", "等MDSP应答超时，网关重发鉴权消息");
        statusDescMap1.put("ID:0087", "因MDSP流控，网关重发鉴权消息");
        statusDescMap1.put("ID:0086", "因MDSP系统忙，且缓存满，网关重发鉴权消息");
        statusDescMap1.put("ID:0085", "因MDSP系统忙，网关重发鉴权消息");
        statusDescMap1.put("ID:0084", "网关向MDSP重发鉴权消息失败");
        statusDescMap1.put("ID:0083", "短消息内容超过了接收侧的最大长度");
        statusDescMap1.put("ID:0082", "循环路由");
        statusDescMap1.put("ID:0081", "发送接收接口重复");
        statusDescMap1.put("ID:0080", "CPCode错误");
        statusDescMap1.put("ID:0079", "业务类型为空");
        statusDescMap1.put("ID:0078", "SPID为空");
        statusDescMap1.put("ID:0077", "超过最大Submit提交数");
        statusDescMap1.put("ID:0076", "信息安全鉴权失败");
        statusDescMap1.put("ID:0075", "送SCP鉴权等待应答超时");
        statusDescMap1.put("ID:0074", "送SCP失败");
        statusDescMap1.put("ID:0073", "等待应答超时");
        statusDescMap1.put("ID:0072", "找不到路由");
        statusDescMap1.put("ID:0071", "超过最大节点数");
        statusDescMap1.put("ID:0070", "网络断连或目的设备关闭接口。（与网关断连）");
        statusDescMap1.put("ID:0069", "此用户为黑名单用户");
        statusDescMap1.put("ID:0068", "用户鉴权失败");
        statusDescMap1.put("ID:0067", "接收服务目的地址鉴权失败");
        statusDescMap1.put("ID:0066", "接收服务源地址鉴权失败");
        statusDescMap1.put("ID:0065", "发送服务目的地址鉴权失败");
        statusDescMap1.put("ID:0064", "发送服务源地址鉴权失败");
        statusDescMap1.put("ID:0063", "不能识别的FeeType");
        statusDescMap1.put("ID:0062", "定时发送时间已经过期");
        statusDescMap1.put("ID:0061", "有效时间已经过期");
        statusDescMap1.put("ID:0056", "用户鉴权时，用户状态不正常");
        statusDescMap1.put("ID:0055", "等待状态报告超时");
        statusDescMap1.put("ID:0054", "超时，未接收到响应消息");
        statusDescMap1.put("ID:0053", "发送消息失败");
        statusDescMap1.put("ID:0052", "尚未成功登录");
        statusDescMap1.put("ID:0051", "尚未建立连接");
        statusDescMap1.put("ID:0049", "超出单次最大群发量");
        statusDescMap1.put("ID:0048", "超出每日最大群发次数");
        statusDescMap1.put("ID:0047", "超出每月最大群发次数");
        statusDescMap1.put("ID:0046", "超出每日最大发送量");
        statusDescMap1.put("ID:0045", "超出每月最大发送量");
        statusDescMap1.put("ID:0044", "消息发送不在有效时间段内");
        statusDescMap1.put("ID:0043", "禁止向异网发送消息");
        statusDescMap1.put("ID:0021", "MDSP用户鉴权模块，用户销户错误");
        statusDescMap1.put("ID:0020", "MDSP用户鉴权模块，鉴权用户停机或欠费错误");
        statusDescMap1.put("ID:0014", "禁止发送WAP PUSH消息");
        statusDescMap1.put("ID:0013", "目的地址错误");
        statusDescMap1.put("ID:0012", "计费地址错误");
        statusDescMap1.put("ID:0011", "Msg_src错误");
        statusDescMap1.put("ID:0010", "Src_ID错误");
        statusDescMap1.put("ID:0009", "infoX-SMS GW不负责此计费号码");
        statusDescMap1.put("ID:0008", "流量控制错误");
        statusDescMap1.put("ID:0007", "业务代码错误");
        statusDescMap1.put("ID:0006", "超过最大信息长度");
        statusDescMap1.put("ID:0005", "资费代码错误");
        statusDescMap1.put("ID:0004", "消息长度错误。或版本太高");
        statusDescMap1.put("ID:0003", "消息序列号重复。或认证错");
        statusDescMap1.put("ID:0002", "命令字错误，或非法源地址");
        statusDescMap1.put("ID:0001", "消息结构错误");
        statusDescMap1.put("ID:0000", "正确");
        statusDescMap1.put("IA:0054", "超时未接收到响应消息");
        statusDescMap1.put("IA:0053", "发送消息失败");
        statusDescMap1.put("IA:0052", "尚未成功登录");
        statusDescMap1.put("IA:0051", "尚未建立连接");
        statusDescMap1.put("DB:0506", "数据库错误");
        statusDescMap1.put("DB:0505", "系统内部错误");
        statusDescMap1.put("DB:0504", "已超过LICENSE限定数量");
        statusDescMap1.put("DB:0503", "LICENSE不合法");
        statusDescMap1.put("DB:0502", "网络故障");
        statusDescMap1.put("DB:0501", "网络链接异常");
        statusDescMap1.put("DB:0500", "磁盘读写错误");
        statusDescMap1.put("DB:0156", "WAPPush消息格式检查错误");
        statusDescMap1.put("DB:0155", "不支持的承载模式");
        statusDescMap1.put("DB:0154", "MServer是断开状态");
        statusDescMap1.put("DB:0153", "信用度错误");
        statusDescMap1.put("DB:0152", "BOSS同步鉴权错误");
        statusDescMap1.put("DB:0151", "正文签名失败");
        statusDescMap1.put("DB:0150", "等M模块应答消息超时");
        statusDescMap1.put("DB:0147", "用户未点播该业务");
        statusDescMap1.put("DB:0146", "用户在SI黑名单中");
        statusDescMap1.put("DB:0145", "用户在EC黑名单中");
        statusDescMap1.put("DB:0144", "用户在全局黑名单中");
        statusDescMap1.put("DB:0143", "超过月最大发送MT数量");
        statusDescMap1.put("DB:0142", "超过日最大发送MT数量");
        statusDescMap1.put("DB:0141", "用户处在黑名单中");
        statusDescMap1.put("DB:0140", "用户不在白名单中");
        statusDescMap1.put("DB:0139", "下发时间段违法");
        statusDescMap1.put("DB:0138", "用户相关信息不存在");
        statusDescMap1.put("DB:0137", "伪码信息错误");
        statusDescMap1.put("DB:0136", "用户密码错误");
        statusDescMap1.put("DB:0135", "业务数据同步出错");
        statusDescMap1.put("DB:0134", "SP数据同步出错");
        statusDescMap1.put("DB:0133", "用户数据同步出错");
        statusDescMap1.put("DB:0132", "相关信息不存在");
        statusDescMap1.put("DB:0131", "BOSS系统数据同步出错");
        statusDescMap1.put("DB:0129", "用户已经是梦网用户");
        statusDescMap1.put("DB:0128", "补款,冲正失败");
        statusDescMap1.put("DB:0127", "该用户没有足够的余额");
        statusDescMap1.put("DB:0126", "该用户不是神州行用户");
        statusDescMap1.put("DB:0125", "业务价格超出范围");
        statusDescMap1.put("DB:0124", "业务价格格式错误");
        statusDescMap1.put("DB:0123", "业务价格为负");
        statusDescMap1.put("DB:0122", "接收异常");
        statusDescMap1.put("DB:0121", "没有该类业务");
        statusDescMap1.put("DB:0120", "话单格式错误");
        statusDescMap1.put("DB:0119", "用户不能取消该业务");
        statusDescMap1.put("DB:0118", "用户已经签约了该业务");
        statusDescMap1.put("DB:0117", "该业务不能对该用户开放");
        statusDescMap1.put("DB:0116", "用户暂停签约该业务");
        statusDescMap1.put("DB:0115", "用户没有签约该业务");
        statusDescMap1.put("DB:0114", "EC/SI暂停服务");
        statusDescMap1.put("DB:0113", "EC/SI不存在");
        statusDescMap1.put("DB:0112", "EC/SI代码错误");
        statusDescMap1.put("DB:0111", "该业务尚未开通");
        statusDescMap1.put("DB:0110", "该服务种类尚未开通");
        statusDescMap1.put("DB:0109", "该服务种类不存在");
        statusDescMap1.put("DB:0108", "该业务暂停服务");
        statusDescMap1.put("DB:0107", "业务不存在");
        statusDescMap1.put("DB:0106", "服务代码错误");
        statusDescMap1.put("DB:0105", "业务代码错误");
        statusDescMap1.put("DB:0104", "用户没有使用该业务的权限");
        statusDescMap1.put("DB:0103", "用户欠费");
        statusDescMap1.put("DB:0102", "用户停机");
        statusDescMap1.put("DB:0101", "手机号码错误");
        statusDescMap1.put("DB:0100", "手机号码不存在");
        statusDescMap1.put("DB:0010", "流量控制错");
        statusDescMap1.put("DB:0009", "超过最大信息长");
        statusDescMap1.put("DB:0008", "资费代码错");
        statusDescMap1.put("DB:0007", "消息长度错");
        statusDescMap1.put("DB:0006", "消息序号重复");
        statusDescMap1.put("DB:0005", "命令字错");
        statusDescMap1.put("DB:0004", "版本太高");
        statusDescMap1.put("DB:0003", "认证错");
        statusDescMap1.put("DB:0002", "非法源地址");
        statusDescMap1.put("DB:0001", "消息结构错");
        statusDescMap1.put("DB:0000", "正确");
        statusDescMap1.put("DA:0054", "超时未接收到响应消息");
        statusDescMap1.put("DA:0053", "发送消息失败");
        statusDescMap1.put("DA:0052", "尚未成功登录");
        statusDescMap1.put("DA:0051", "尚未建立连接");
        statusDescMap1.put("IC:0151", "SMSC没有给本网关回状态报告");
        statusDescMap1.put("IC:0001", "SMSC没有给本网关回状态报告");
        statusDescMap1.put("IA:0073", "可能是异地延迟");
        statusDescMap1.put("MB:0255", "因信息内容涉及安全字眼被过滤，请修改内容后重新提交");
        statusDescMap1.put("MK:0118", "如果是号码是异地的，要打号码所在地的10086,报障查询");
        statusDescMap1.put("sk:8001", "北京企信通黑名单");
        statusDescMap1.put("sk:8602", "屏蔽审核");
        statusDescMap1.put("sk:8300", "余额不足");
        statusDescMap1.put("sk:8101", "通道暂停");
        statusDescMap1.put("sk:8201", "业务暂停");
        statusDescMap1.put("MM:0064", "空号");
        statusDescMap1.put("MM:0174", "通道黑名单或敏感字");
        statusDescMap1.put("MK:0118", "通道黑名单或敏感字");
        statusDescMap1.put("MK:0115", "通道黑名单或敏感字");
        statusDescMap1.put("MK:0036", "通道黑名单或敏感字");
        statusDescMap1.put("MN:0001", "空号");
        statusDescMap1.put("MN:0011", "可能停机");
        statusDescMap1.put("ID:0011", "企业代码错");
        statusDescMap1.put("MB:1031", "不是网关错误代码");
        statusDescMap1.put("IB:0012", "携号转出，拒绝消息");
        statusDescMap1.put("DB:0164", "对方上行了什么退订命令到端口");
        statusDescMap1.put("ID:0198", "非模板发内容");
        statusDescMap1.put("MC:0055", "状态报告超时");
        statusDescMap1.put("IB:0011", "通道地区数据被删除，除当地运营商重新增加数据");
        statusDescMap1.put("IB:0064", "通道地区数据被删除，除当地运营商重新增加数据");
        statusDescMap1.put("MN:0017", "手机内存已满");
        statusDescMap1.put("MI:0000", "空号");
        statusDescMap1.put("MI:0004", "空号");
        statusDescMap1.put("HD:0001", "第三方合作商信达黑名单");
        statusDescMap1.put("MB：0069", "短信中心的队列阻塞/部分敏感字拦截");
        statusDescMap1.put("MH:0007", "HCT敏感字拦截");
        statusDescMap1.put("MK:0012", "空号/停机");
        statusDescMap1.put("MK:0011", "消息等待队列满");
        statusDescMap1.put("MK:0010", "SM发送失败");
        statusDescMap1.put("MK:0005", "呼叫被禁止");
        statusDescMap1.put("MK:0004", "电信业务不支持");
        statusDescMap1.put("MK:0001", "未知用户");
        statusDescMap1.put("MK:0000", "正常");
        statusDescMap1.put("MB:1042", "要下发给被叫用户的短消息数超过了该用户的最大下发");
        statusDescMap1.put("MM:0064", "空号");
        statusDescMap1.put("MN:0001", "空号");
        statusDescMap1.put("MN:0011", "可能停机");
        statusDescMap1.put("MN:0017", "可能停机");
        statusDescMap1.put("MI:0005", "可能停机");
        statusDescMap1.put("MN:0000", "空号");
        statusDescMap1.put("NOROUTE", "路由错误");
        statusDescMap1.put("SUBMIT_FAILD", "玄武提交到运营商网关失败");
    }

}
