/**
 * 
 */
package io.potato.ts.sms.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import io.potato.core.util.SmsUtil;
import io.potato.core.util.Strings;
import io.potato.ts.sms.SmsContent;
import io.potato.ts.sms.SmsResponse.SmsID;
import io.potato.ts.sms.SmsSender;
import lombok.extern.slf4j.Slf4j;

/**
 * @author timl
 * created: 2019年2月22日 下午6:20:49
 */
@Slf4j
//@Component
public class EmptySender implements SmsSender {

	@Override
	public List<SmsID> batchSendSms(String to, String body, String extendCode, String extendStr, String sign) {
		
		String[] arr = to.split(",");
		log.warn("SEND " + arr.length + " SAME SMS.");
		List<SmsID> list = new ArrayList<>();
		for (String str : arr) {
			SmsID smsID = new SmsID();
			smsID.setOriginTo(SmsUtil.formatPhoneNumber(str));
			smsID.setSmsMsgId(Strings.uuid());
			list.add(smsID);
		}
		sleep();
		return list;
	}

	@Override
	public List<SmsID> batchSendDiffSms(List<SmsContent> smsList, String extendCode, String extendStr, String sign) {
		log.warn("SEND " + smsList.size() + " DIFF SMS.");
		List<SmsID> list = new ArrayList<>();
		smsList.forEach(s -> {
			for (String num : s.getTo()) {
				SmsID smsID = new SmsID();
				smsID.setOriginTo(SmsUtil.formatPhoneNumber(num));
				smsID.setStatus("000000");
				smsID.setSmsMsgId(Strings.uuid());
				list.add(smsID);
			}
		});
		sleep();
		return list;
	}
	
	private void sleep() {
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
