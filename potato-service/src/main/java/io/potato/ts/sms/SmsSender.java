/**
 * 
 */
package io.potato.ts.sms;

import java.util.List;

import io.potato.ts.sms.SmsResponse.SmsID;

/**
 * 发送短信
 * @author timl
 * created: 2019年1月8日 下午3:52:26
 */
public interface SmsSender {
	
	/**
	 * 批量发送短信
	 * @param to  短信接收方的号码  如果携带多个接收方号码，则以英文逗号分隔。每个号码最大长度为21位，最多允许携带1000个号码
	 * @param body  短信内容  最大长度为2000字节，国内短信必须在短信内容中携带短信签名
	 * @param extendCode  拓展码， 最大7位
	 * @param extendStr  扩展信息
	 * @param sign 短信签名
	 * @return  每条短信的短信ID 发送结果等信息
	 */
	List<SmsID> batchSendSms(String to, String body, String extendCode, String extendStr, String sign);
	
	/**
	 * 批量发送不同短信 
	 * @param smsList  每条短信的内容和接收号码
	 * @param extendCode  拓展码， 最大7位
	 * @param extendStr  扩展信息
	 * @param sign 短信签名
	 * @return 每条短信的短信ID 发送结果等信息
	 */
	List<SmsID> batchSendDiffSms(List<SmsContent> smsList, String extendCode, String extendStr, String sign);
}
