package io.potato.ts.sms;

import java.util.List;

import io.potato.core.util.SmsUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmsContent {
	private String[] to;
	private String body;
	
	public SmsContent() {
		super();
	}
	
	public SmsContent(String to, String body) {
		this.to = new String[] {SmsUtil.trimPhoneNumber(to)};
		this.body = body;
	}
	
	public SmsContent(List<String> list, String body) {
		if (list != null && list.size() > 0) {
			to = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				to[i] = list.get(i);
			}
		}
		this.body = body;
	}
}
