/**
 * 
 */
package io.potato.ts.sms;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 发送分批短信接口返回数据
 * @author timl
 * created: 2019年1月9日 上午9:42:45
 */
@Getter
@Setter
@ToString
public class SmsResponse {
	
	/**
	 * 请求返回的结果码。
	 */
	private String code;
	
	/**
	 * 请求返回的结果码描述
	 */
	private String description;
	
	/**
	 * 短信结构体，当返回响应出现异常时不包含此字段
	 */
	private List<SmsID> result;
	
	
	@Getter
	@Setter
	@ToString
	public static class SmsID {
		private String smsMsgId;
		
		/**
		 * 短信发送方的号码
		 */
		private String from;
		
		/**
		 * 短信接收方的号码。
		 */
		private String originTo;
		
		/**
		 * 短信状态码。
 000000：成功
 E200015：待发送短信数量太大
 E200028：模板校验失败
 E200029：模板匹配失败
 E200030：模板未激活
 E200031：协议校验失败
 E200033：模板类型不正确
		 */
		private String status;
		
		/**
		 *  短信资源的创建时间，即短信平台接收到SP
			发送短信请求的时间。
			格式为：YYYY-MM-DD'T'hh:mm:ss'Z'。
		 */
		private String createTime;
		
		/*public SmsID() {
			super();
		}

		public SmsID(String smsMsgId, String from, String originTo, String status, String createTime) {
			super();
			this.smsMsgId = smsMsgId;
			this.from = from;
			this.originTo = originTo;
			this.status = status;
			this.createTime = createTime;
		}*/
		
		
	}

}
