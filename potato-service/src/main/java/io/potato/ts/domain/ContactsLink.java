package io.potato.ts.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.potato.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通讯录群组和通讯录联系人关系表
 * NOTICE： this file is generated by code, DO NOT modify it!
 * @author timl
 *
 * <p>2019-01-07 17:45:17</p>
 */
 
@Entity
@Table(name="t_contacts_link")
@Data
@EqualsAndHashCode(callSuper=false)
public class ContactsLink extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1546854317179L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 
	 */
	@Column(name="contracts_id")
	private Integer contractsId;
	
	/**
	 * 
	 */
	@Column(name="group_id")
	private Integer groupId;

	/**
	 * 所属用户ID
	 */
	@Column(name="user_id")
	private Integer userId;

	/**
	 * 单位ID
	 */
	@Column(name="organ_id")
	private Integer organId;
	
	/**
	 * 
	 */
	@Column(name="created_at", insertable=false, updatable=false)
	private LocalDateTime createdAt;
	
	/**
	 * 
	 */
	@Column(name="updated_at", insertable=false, updatable=false)
	private LocalDateTime updatedAt;
	
}