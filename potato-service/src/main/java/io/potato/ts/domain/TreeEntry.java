package io.potato.ts.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 树形结构entry
 * 
 * @author longshangbo@163.com
 *
 * <p>2018年8月1日 下午8:59:07</p>
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class TreeEntry extends Entry<Integer> {

	private static final long serialVersionUID = 7708811107752235233L;
	
	public static final int ROOT = -1;

	private Integer pValue;

	public TreeEntry(String display, Integer value, Integer pValue) {
		super(display, value);
		this.pValue = pValue;
	}
	
}
