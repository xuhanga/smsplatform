package io.potato.ts.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.potato.core.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 运营商
 * NOTICE： this file is generated by code, DO NOT modify it!
 * @author timl
 *
 * <p>2019-01-07 17:45:17</p>
 */
 
@Entity
@Table(name="t_dealer")
@Data
@EqualsAndHashCode(callSuper=false)
public class Dealer extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1546854317212L;

	/**
	 * 
	 */
	@Id
	@Column(name="dealer_id")
	private Integer dealerId;
	
	/**
	 * 
	 */
	@Column(name="dealer_name")
	private String dealerName;
	
}