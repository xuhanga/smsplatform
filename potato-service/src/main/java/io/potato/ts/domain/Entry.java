package io.potato.ts.domain;

import java.io.Serializable;

import lombok.Data;

/**
 * 键值对结构
 * 
 * @author longshangbo@163.com
 *
 * <p>2017年7月22日 下午4:01:58</p>
 */
@Data
public class Entry<T> implements Serializable{
	
	private static final long serialVersionUID = -142027186585665785L;
	
	private String display;
	private T value;
	
	public Entry(String display, T value) {
		this.display = display;
		this.value = value;
	}
	
}
