package io.potato.ts.common.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 发送短信参数
 * author: timl
 * time: 2019-4-12 9:21
 */
@Data
public class SmsSendParam {

    /**
     * 个人通讯录群组ID
     */
    private List<Integer> contactsGroupIdList;

    /**
     * 共享通讯录群组ID
     */
    private List<Integer> shareContactsGroupIdList;

    /**
     * 号码组ID
     */
    private List<Integer> userGroupIdList;

    /**
     * 号码文件ID
     */
    private List<Integer> mobileFileIdList;

    /**
     * 号码
     */
    private List<String> numberList;

    /**
     * 短信内容
     */
    private String content;

    /**
     * 接收报告
     */
    private boolean needReport;

    /**
     * 需要回复
     */
    private boolean needReply;

    /**
     * 附加姓名
     */
    private boolean addName;

    /**
     * 附加账号信息
     */
    private boolean addAccountInfo;

    /**
     * 优先级
     */
    private Integer priority;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;
}
