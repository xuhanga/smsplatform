/**
 * 
 */
package io.potato.ts.common.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author timl
 * created: 2019年2月14日 下午2:30:37
 */
@Getter
@Setter
@ToString
public class SmsInfo {
	
	private Long id;
	
	private String msgId = "";
	
	private String userCode;
	
	private String batchCode;
	
	private String extendCode;
	
	private String mobile;
	
	private String content;
	
	private String taskName;
	
	private LocalDateTime submitTime;
	
	private Integer needReport;
	
	private Integer smsType;
	
	public SmsInfo() {
		super();
	}

	public SmsInfo(String extendCode, String mobile, String content) {
		super();
		this.extendCode = extendCode;
		this.batchCode = extendCode;
		this.mobile = mobile;
		this.content = content;
	}
	
	public SmsInfo(Long id, String msgId, String userCode, String batchCode, 
			String extendCode, String mobile, String content, 
			LocalDateTime submitTime, Integer needReport,
			String taskName, Integer smsType) {
		this(extendCode, mobile, content);
		this.id = id;
		this.userCode = userCode;
		this.batchCode = batchCode;
		this.msgId = msgId;
		this.submitTime = submitTime;
		this.needReport = needReport;
		this.taskName = taskName;
		this.smsType = smsType;
	}
	
}
