/**
 * 
 */
package io.potato.ts.common;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.potato.core.util.IdWorker;
import lombok.extern.slf4j.Slf4j;

/**
 * @author timl
 * created: 2019年3月15日 上午11:34:55
 */
@Component
@Slf4j
public class SmsIdWorker {
	
	@Value("${app.worker-id}")
	private Long workerId;
	
	/**
	 * ID生成器
	 */
	private IdWorker idWorker;
	
	@PostConstruct
	public void init() {
		log.info("idWorker id " + workerId);
		this.idWorker = new IdWorker(workerId);
	}
	
	public Long nextId() {
		return idWorker.nextId();
	}
	
}
