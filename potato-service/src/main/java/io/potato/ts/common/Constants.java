/**
 * 
 */
package io.potato.ts.common;

/**
 * 常量定义
 * @author timl
 * created: 2019年1月9日 下午6:15:53
 */
public interface Constants {
	
	int SEND_WORKER_NUM = 20;
	
	int SEND_WEB_SMS_NUM = 10;
	
	/**
	 * 短信发送人类型：API
	 */
	Integer SENDER_TYPE_API = 1;
	
	/**
	 * 短信发送人类型：WEB
	 */
	Integer SENDER_TYPE_WEB = 2;
	
	Integer SENDING_SEQ_FOR_WEB = 100;
	
	/**
	 * 默认每页显示记录数
	 */
	int DEFAULT_ROWS = 20;
	
	/**
	 * 角色
	 */	
	public static class Role {
		/**
		 * 普通用户
		 */
		public static final Integer USER = 1;
		
		/**
		 * 单位管理员
		 */
		public static final Integer ADMIN = 2;
		
		/**
		 * 超级管理员
		 */
		public static final Integer SUPERADMIN = 4;
		
		public static final String NAME_USER = "user";
		public static final String NAME_ADMIN = "admin";
		public static final String NAME_SUPERADMIN = "superadmin";
	}
	
	
	
	// 缓存
	
	/**
	 * 配置缓存
	 */
	String CACHE_CONFIG = "sms_config";
	
	/**
	 * API Session
	 */
	String CACHE_API_SESSION = "sms_api_session";
	
	
	/**
	 * 待发短信的状态
	 */
	public static class SendingStatus {
		/**
		 * 等待发送
		 */
		public static final Integer TO_BE_SEND = 0;
		
		/**
		 * 已发送
		 */
		public static final Integer SENDED = 1;
		
		/**
		 * 已暂停
		 */
		public static final Integer PAUSE = 2;
		
		/**
		 * 已取消
		 */
		public static final Integer CANCELED = -1;
		
		/**
		 * 已删除
		 */
		public static final Integer DELETED = -2;
	}
	
	/**
	 * 短信类型
	 * @author timl
	 * created: 2019年2月21日 下午3:10:39
	 */
	public static class SmsType {
		
		/**
		 * 普通短信
		 */
		public static final Integer NORMAL = 0;
		
		/**
		 * 定时短信
		 */
		public static final Integer SCHEDULE = 1;
		
		/**
		 * 自动回复短信
		 */
		public static final Integer REPLY = 2;
		
	}
	
	/**
	 * 运营商
	 * @author timl
	 * created: 2019年2月22日 上午9:58:44
	 */
	public static class Dealer {
		
		/**
		 * 中国移动
		 */
		public static final Integer CHINA_MOBILE = 1;
		
		/**
		 * 中国电信
		 */
		public static final Integer CHINA_TELECOM = 2;
		
		/**
		 * 中国联通
		 */
		public static final Integer CHINA_UNION = 3;
		
	}
	
	/**
	 * 是否需要状态报告
	 * created: 2019年2月25日 下午2:35:25
	 */
	public static class NeedReport {
		
		/**
		 * 需要状态报告
		 */
		public static final Integer YES = 1;
		
		/**
		 * 不需要状态报告
		 */
		public static final Integer NO = 0;
	}
	
	/**
	 * 上行短信状态
	 * @author timl
	 * created: 2019年3月12日 上午11:59:06
	 */
	public static class UpRecordStatus {
		/**
		 * 正常
		 */
		public static final Integer NORMAL = 0;
		
		/**
		 * 已废弃的
		 */
		public static final Integer DISCARDED = 1;
	}

	/**
	 * 通讯录类型
	 * 
	 * @author longshangbo@163.com
	 *
	 * <p>2019年4月12日 上午2:23:16</p>
	 */
	public static class GroupType {
		
		/**
		 * 私有的
		 */
		public static final Integer PRIVATE = 1;
		
		/**
		 * 共享的
		 */
		public static final Integer SHARED = 2;
		
	}
}
