/**
 * 
 */
package io.potato.ts.config;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * RestTemplate 配置
 * @author timl
 * created: 2018年11月2日 上午9:36:06
 */
@Configuration
public class HttpClientConfig {

    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory factory){
        return new RestTemplate(factory);
    }

    //@Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
    	// JDK标准实现
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(5000);//单位为ms
        factory.setConnectTimeout(5000);//单位为ms
        return factory;
    }
    
    //@Bean
    /*public ClientHttpRequestFactory HttpComponentsClientHttpRequestFactory() {
    	// apache httpclient 实现
    	PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(100);
        connectionManager.setMaxTotal(100);
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
                HttpClientBuilder.create().setConnectionManager(connectionManager).build()
        );
        
        factory.setConnectTimeout(5000);//单位为ms
        factory.setReadTimeout(5000);//单位为ms
        return factory;
    }*/
    
    @Bean
    public ClientHttpRequestFactory okHttp3ClientHttpRequestFactory() {
    	// OKHttp 实现
    	OkHttpClient client = new OkHttpClient.Builder()
    			.connectionPool(new ConnectionPool(100, 5, TimeUnit.MINUTES))
    			.connectTimeout(30, TimeUnit.SECONDS)
    			.readTimeout(30, TimeUnit.SECONDS)
    			.writeTimeout(30, TimeUnit.SECONDS)
    			.retryOnConnectionFailure(true)
    			//.sslSocketFactory(sslSocketFactory(), x509TrustManager())
    			.build();
    	return new OkHttp3ClientHttpRequestFactory(client);
    }
    
    //@Bean
    public X509TrustManager x509TrustManager() {
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }
            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
    }
    
    //@Bean
    public SSLSocketFactory sslSocketFactory() {
        try {
            //信任任何链接
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{x509TrustManager()}, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }
    
}