/**
 * 
 */
package io.potato.ts.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author timl
 * created: 2019年1月22日 下午4:15:12
 */
@Configuration
@EnableAsync
public class TaskConfig {
	
	@Bean
    public AsyncTaskExecutor taskExecutor() {  
   	 	ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(100);
        executor.setMaxPoolSize(100);
        //executor.setQueueCapacity(20);
        executor.setThreadNamePrefix("sms-task-");
        executor.initialize();
        return executor;    	 
    }
}
