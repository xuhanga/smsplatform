package io.potato.ts.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.common.Constants;
import io.potato.ts.domain.CodeSeg;
import io.potato.ts.repository.CodeSegRepository;


/**
 * 手机号码号段运营商对应表服务类
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
@Service
@Transactional
public class CodeSegService extends CrudService<CodeSeg, Integer> {

	@Autowired
	CodeSegRepository repository;

	@Override
	protected JpaRepository<CodeSeg, Integer> getRepository() {
		return repository;
	}
	
	@Cacheable(value = Constants.CACHE_CONFIG)
	public Map<String, Integer> findMap() {
		List<CodeSeg> list = this.findAll();
		Map<String, Integer> map = new HashMap<>();
		list.forEach(s -> map.put(s.getCode(), s.getDealerId()));
		return map;
	}
	
}
