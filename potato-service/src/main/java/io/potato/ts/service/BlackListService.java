package io.potato.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.domain.BlackList;
import io.potato.ts.domain.VDept;
import io.potato.ts.repository.BlackListRepository;


/**
 * 黑名单号码表服务类
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:56</p>
 */
@Service
@Transactional
public class BlackListService extends CrudService<BlackList, Integer> {

	@Autowired
	BlackListRepository repository;

	@Override
	protected JpaRepository<BlackList, Integer> getRepository() {
		return repository;
	}
	
	/**
	 * 根据号码查找没名单
	 * @param pageable  分页排序
	 * @param mobileNo  手机号码
	 * @return
	 */
	public Page<BlackList> findByMobileNo(Pageable pageable, String mobileNo) {
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("mobileNo", GenericPropertyMatchers.contains());
		
		BlackList obj = new BlackList();
		obj.setMobileNo(mobileNo);
		
		return repository.findAll(Example.of(obj, exampleMatcher), pageable);
	}
	
}
