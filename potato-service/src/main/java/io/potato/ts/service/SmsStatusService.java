package io.potato.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.domain.SmsStatus;
import io.potato.ts.repository.SmsStatusRepository;


/**
 * 服务类
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
@Service
@Transactional
public class SmsStatusService extends CrudService<SmsStatus, Integer> {

	@Autowired
	SmsStatusRepository repository;

	@Override
	protected JpaRepository<SmsStatus, Integer> getRepository() {
		return repository;
	}
	
}
