package io.potato.ts.service;

import io.potato.core.AppException;
import io.potato.core.CrudService;
import io.potato.core.exception.NoDataAccessRightException;
import io.potato.core.util.SmsUtil;
import io.potato.core.util.StringUtils;
import io.potato.ts.domain.MobileFile;
import io.potato.ts.repository.MobileFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * 号码文件表服务类
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
@Service
@Transactional
public class MobileFileService extends CrudService<MobileFile, Integer> {

	@Autowired
	MobileFileRepository repository;

	@Override
	protected JpaRepository<MobileFile, Integer> getRepository() {
		return repository;
	}
	
	public Page<MobileFile> findByName(Pageable pageable, String name, Integer userId) {
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("name", GenericPropertyMatchers.contains());
		
		MobileFile obj = new MobileFile();
		obj.setName(name);
		obj.setUserId(userId);
		
		return repository.findAll(Example.of(obj, exampleMatcher), pageable);
	}

	/**
	 * 查询用户指定ID的号码文件列表
	 * @param ids  号码文件ID
	 * @param userId  用户ID
	 * @return 号码文件列表
	 */
	public List<MobileFile> findByIds(List<Integer> ids, Integer userId) {
		if (ids == null || ids.isEmpty()) {
			return new ArrayList<>();
		}
		return repository.findByIds(ids, userId);
	}

    /**
     * 保存号码文件
     * @param mobileFile
     * @return
     */
	public MobileFile save(MobileFile mobileFile) {

	    if (mobileFile.getId() == null) {
	        return super.save(mobileFile);
        }

	    Optional<MobileFile> optionalMobileFile = this.findById(mobileFile.getId());
	    if (optionalMobileFile.isPresent()) {
            MobileFile mf = optionalMobileFile.get();

            if (!mf.getUserId().equals(mobileFile.getUserId())) {
            	throw new NoDataAccessRightException();
			}

            mf.setName(mobileFile.getName());
            mf.setNums(mobileFile.getNums());
            super.save(mf);
            return mf;
        } else {
            return mobileFile;
        }
    }

    public List<String> checkNums(String nums) {
	    if (StringUtils.isBlank(nums)) {
	        throw new AppException("号码不能为空");
        }

        String[] arr = nums.split("[ \r\n]+");
	    return checkNums(arr);
    }

    public List<String> checkNums(String[] numArr) {
		List<String> list = new ArrayList<>();
		for (String num : numArr) {
			if (!SmsUtil.isPhoneNumber(num)) {
				list.add(num);
			}
		}
		return list;
	}

	public List<String> checkNums(List<String> numList) {
		List<String> list = new ArrayList<>();
		for (String num : numList) {
			if (!SmsUtil.isPhoneNumber(num)) {
				list.add(num);
			}
		}
		return list;
	}


	public void deleteByIds(Integer[] ids, Integer userId) {
		repository.deleteByIds(ids, userId);
	}

	/**
	 * 查询用户所有的号码文件列表
	 * @param userId 用户ID
	 * @return  号码文件列表
	 */
	public List<MobileFile> findByUserId(Integer userId) {
		return repository.findByUserId(userId);
	}
	
}
