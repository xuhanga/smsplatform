package io.potato.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import io.potato.ts.domain.SmsStatusReport;

@Service
public class AsyncService {
	
	@Autowired
	SmsStatusReportService ssr;
	
	@Async
	public void receiveReport(SmsStatusReport smsReport) {
		ssr.receive(smsReport);
	}
	
}
