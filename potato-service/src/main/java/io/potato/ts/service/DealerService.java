package io.potato.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.domain.Dealer;
import io.potato.ts.repository.DealerRepository;


/**
 * 运营商服务类
 * 
 * @author timl
 *
 * <p>2019-01-07 15:13:58</p>
 */
@Service
@Transactional
public class DealerService extends CrudService<Dealer, Integer> {

	@Autowired
	DealerRepository repository;

	@Override
	protected JpaRepository<Dealer, Integer> getRepository() {
		return repository;
	}
	
}
