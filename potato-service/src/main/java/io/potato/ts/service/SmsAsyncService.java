package io.potato.ts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import io.potato.ts.domain.SmsStatusReport;

@Service
public class SmsAsyncService {
	
	@Autowired
	SmsStatusReportService statusReport;
	
	/**
	 * 更新短信报告为已读， 已读的短信报告不会被查询到
	 * @param list  
	 */
	@Async
	public void updateReadStatus(List<SmsStatusReport> list) {
		this.statusReport.updateReadStatus(list);
	}
}
