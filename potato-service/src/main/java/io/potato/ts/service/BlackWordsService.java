package io.potato.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.domain.BlackList;
import io.potato.ts.domain.BlackWords;
import io.potato.ts.repository.BlackWordsRepository;


/**
 * 禁用词语表服务类
 * 
 * @author timl
 *
 * <p>2019-03-19 14:14:57</p>
 */
@Service
@Transactional
public class BlackWordsService extends CrudService<BlackWords, Integer> {

	@Autowired
	BlackWordsRepository repository;

	@Override
	protected JpaRepository<BlackWords, Integer> getRepository() {
		return repository;
	}
	
	/**
	 * 查找禁用词语
	 * @param pageable  分页排序
	 * @param words  禁用词
	 * @return
	 */
	public Page<BlackWords> findByWords(Pageable pageable, String words) {
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("words", GenericPropertyMatchers.contains());
		
		BlackWords obj = new BlackWords();
		obj.setWords(words);
		
		return repository.findAll(Example.of(obj, exampleMatcher), pageable);
	}
	
}
