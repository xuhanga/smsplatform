package io.potato.ts.service;

import io.potato.core.AppException;
import io.potato.core.exception.NoDataAccessRightException;
import io.potato.ts.domain.User;
import io.potato.ts.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.potato.core.CrudService;
import io.potato.ts.domain.Dept;
import io.potato.ts.domain.VDept;
import io.potato.ts.repository.DeptRepository;
import io.potato.ts.repository.VDeptRepository;

import java.util.List;
import java.util.Optional;


/**
 * 部门表服务类
 * 
 * @author timl
 *
 * <p>2019-03-11 18:22:11</p>
 */
@Service
@Transactional
public class DeptService extends CrudService<Dept, Integer> {

	@Autowired
	DeptRepository repository;
	
	@Autowired
	VDeptRepository vrepository;

	@Autowired
	UserRepository userRepository;

	@Override
	protected JpaRepository<Dept, Integer> getRepository() {
		return repository;
	}
	
	/**
	 * 根据名称查找单位（模糊匹配）
	 * @param pageable  分页排序信息
	 * @param name  单位名称
	 * @return  单位列表分页
	 */
	public Page<VDept> findByName(Pageable pageable, String name, Integer organId) {
		
		if (organId == null || organId <=0) {
			throw new IllegalArgumentException("organId 不能为空");
		}
		
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("deptName", GenericPropertyMatchers.contains());
		
		VDept obj = new VDept();
		obj.setOrganId(organId);
		if (StringUtils.isNotBlank(name)) {
			obj.setDeptName(name);
		}
		
		return vrepository.findAll(Example.of(obj, exampleMatcher), pageable);
		
	}

	/**
	 * 查询单位下所有部门
	 * @param organId 单位ID
	 * @return
	 */
	public List<Dept> findByOrganId(Integer organId) {
		return repository.findByOrganId(organId);
	}

	/**
	 * 保存部门的数据
	 * @param dept  部门
	 * @param organId  单位ID
	 */
	public void save(Dept dept, Integer organId) {
		if (dept.getId() == null) {
			dept.setOrganId(organId);
			super.save(dept);
		} else {
			Optional<Dept> optionalDept = repository.findById(dept.getId());
			if (optionalDept.isPresent()) {
				Dept oldDept = optionalDept.get();

				if (!oldDept.getOrganId().equals(organId)) {
					// 只能修改自己单位的数据
					throw new NoDataAccessRightException();
				}

				oldDept.setDeptName(dept.getDeptName());
				oldDept.setShowOrder(dept.getShowOrder());
				super.save(oldDept);
			} else {
				throw new AppException("记录不存在");
			}
		}
	}

	/**
	 * 删除部门
	 * @param ids  部门ID
	 * @param organId  单位ID
	 */
	public void deleteByIds(Integer[] ids, Integer organId) {
		List<User> userList = userRepository.findByDeptIds(ids);
		if (userList.size() > 0) {
			throw new AppException("部门有关联的用户，不能删除");
		}
		repository.deleteByIds(ids, organId);
	}

	
}
