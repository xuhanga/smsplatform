package io.potato.ts.webservice.model;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import io.potato.core.util.xml.DateTimeAdapter;
import lombok.Data;

/**
 * 每条短信内容
 * @author timl
 * created: 2019年1月9日 下午6:22:25
 */
@Data
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class SmsMessage {
	
	/**
	 * 源地址,可为空,在现有系统管理员分配号码上再扩展号码.
	 * 比如:移动分配号码为:106573000573,
	 * 系统管理员分配号码为:08,
	 * 该节点填写号码为05,
	 * 则下发的号码为:1065730005730805
	 */
	@XmlElement
	private String orgaddr;
	
	/**
	 * 手机号码,多个手机号码公用<content>节点,则用英文逗号(,)隔开
	 */
	@XmlElement
	private String mobile;
	
	/**
	 * 短信内容
	 */
	@XmlElement
	private String content;
	
	/**
	 * 要求下发时间,可为空,为空则立即下发.格式:yyyyMMddhhmmss
	 */
	@XmlElement
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private LocalDateTime sendtime;
	
	/**
	 * 是否要状态报告,0,不要,1,要
	 */
	@XmlElement
	private Integer needreport = 0;
}
