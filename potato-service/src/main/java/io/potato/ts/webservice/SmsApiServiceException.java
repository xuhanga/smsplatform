/**
 * 
 */
package io.potato.ts.webservice;

/**
 * Web Service 异常
 * @author timl
 * created: 2019年1月15日 上午10:01:57
 */
public class SmsApiServiceException extends Exception {

	private static final long serialVersionUID = 7165248970509506667L;
	
	private Integer code;
	
	public SmsApiServiceException() {
		super();
	}
	
	public SmsApiServiceException(String message) {
		super(message);
	}
	
	public SmsApiServiceException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public SmsApiServiceException(Throwable cause) {
        super(cause);
    }
	
	public SmsApiServiceException(Integer code, String message) {
		super(message);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
