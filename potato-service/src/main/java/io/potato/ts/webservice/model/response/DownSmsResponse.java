/**
 * 
 */
package io.potato.ts.webservice.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 下发短信
 * @author timl
 * created: 2019年1月09日 上午11:49:54
 */

@XmlRootElement(name = "response")
public class DownSmsResponse extends Response {
	
	private Body body;
	
	public DownSmsResponse() {
		
	}
	
	public DownSmsResponse(List<String> msgid) {
		super();
		this.body = new Body(msgid);
	}
	
	public DownSmsResponse(Integer code, String message) {
		super(code, message);
	}
	
	public DownSmsResponse(Integer code, String message, List<String> msgid) {
		super(code, message);
		this.body = new Body(msgid);
	}
	
	@XmlElement
	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	@XmlRootElement(name = "body")
	static class Body {
		
		private List<String> msgid;
		
		private String reserve = "";
		
		public Body() {
			
		}
		
		public Body(List<String> msgid) {
			this.msgid = msgid;
		}
		
		@XmlElement
		public List<String> getMsgid() {
			return msgid;
		}

		public void setMsgid(List<String> msgid) {
			this.msgid = msgid;
		}
		
		@XmlElement
		public String getReserve() {
			return reserve;
		}

		public void setReserve(String reserve) {
			this.reserve = reserve;
		}
	}
}
