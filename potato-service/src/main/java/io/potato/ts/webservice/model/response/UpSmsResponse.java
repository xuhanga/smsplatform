/**
 * 
 */
package io.potato.ts.webservice.model.response;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import io.potato.core.util.xml.DateTimeAdapter;
import lombok.Getter;
import lombok.Setter;

/**
 * 上行短信
 * @author timl
 * created: 2019年1月09日 上午11:03:04
 */
@XmlRootElement(name = "response")
public class UpSmsResponse extends Response {
	
	private List<Sms> sms;
	
	@XmlElement
	@XmlElementWrapper(name = "body")
	public List<Sms> getSms() {
		return sms;
	}

	public void setSms(List<Sms> sms) {
		this.sms = sms;
	}
	
	public UpSmsResponse() {
		
	}
	
	public UpSmsResponse(List<Sms> sms) {
		super();
		this.sms = sms;
	}
	
	public UpSmsResponse(Integer code, String message, List<Sms> sms) {
		super(code , message);
		this.sms = sms;
	}
	
	public UpSmsResponse(Integer code, String message) {
		super(code , message);
	}

	@Getter
	@Setter
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Sms {
		
		/**
		 * 手机号
		 */
		@XmlElement
		private String telno;
		
		/**
		 * 端口号
		 */
		@XmlElement
		private String destaddr;
		
		/**
		 * 短信内容
		 */
		@XmlElement
		private String content;
		
		/**
		 * 短信唯一标识
		 */
		@XmlElement
		private String msgid;
		
		/**
		 * MAS机接收短信时间
		 */
		@XmlElement
		@XmlJavaTypeAdapter(DateTimeAdapter.class)
		private LocalDateTime receivetime;
		
		/**
		 * 保留项，暂时未用
		 */
		@XmlElement
		private String reserve = "";

		public Sms(String telno, String destaddr, String content, String msgid, LocalDateTime receivetime) {
			super();
			this.telno = telno;
			this.destaddr = destaddr;
			this.content = content;
			this.msgid = msgid;
			this.receivetime = receivetime;
		}
		
		
	}
}
