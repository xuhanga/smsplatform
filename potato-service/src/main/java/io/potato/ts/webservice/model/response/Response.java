/**
 * 
 */
package io.potato.ts.webservice.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * webservice响应结果
 * @author timl
 * created: 2019年1月9日 上午10:38:47
 */
@XmlRootElement(name = "response")
public class Response {
	
	private Head head;
	
	
	@XmlElement
	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}

	public Response() {
		this.head = new Head(0, "success");
	}
	
	public Response(Head head) {
		this.head = head;
	}
	
	public Response(Integer code, String message) {
		this.head = new Head(code, message);
	}
	
	/**
	 * 响应头
	 */
	public static class Head {
		
		/**
		 * 0成功， 其他失败
		 */
		private Integer code;
		
		/**
		 * 结果描述
		 */
		private String message;
		
		public Head() {
			
		}
		
		public Head(Integer code, String message) {
			this.code = code;
			this.message = message;
		}
		
		@XmlElement
		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}
		
		@XmlElement
		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
		
	}
	
}
