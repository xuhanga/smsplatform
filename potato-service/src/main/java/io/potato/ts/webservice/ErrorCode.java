/**
 * 
 */
package io.potato.ts.webservice;

/**
 * @author timl
 * created: 2019年1月15日 上午10:09:02
 */
public final class ErrorCode {
	
	public static final Integer SERVER_ERROR = 1;
	
	public static final Integer USER_PASS_INVALID = 2;
	
	public static final Integer PARAM_INVALID = 3;
	
}
