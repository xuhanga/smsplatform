/**
 * 
 */
package io.potato.ts.webservice.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * 发送短信信息
 * xml 格式如下：
<sendbody>
	<message>
	<orgaddr>50</orgaddr><mobile>13823549398</mobile><content>李老师</content><sendtime>20110428101715</sendtime><needreport>0</needreport>
	</message>
	<message>
	<orgaddr></orgaddr><mobile>13777778888,13777778889</mobile><content>杨老师</content><sendtime></sendtime>
	</message>
	<publicContent>,今天下午举行活动</publicContent>
</sendbody>
 * @author timl
 * created: 2019年1月9日 下午6:20:34
 */
@Data
@XmlRootElement(name = "sendbody")
@XmlAccessorType(XmlAccessType.FIELD)
public class SmsSendBody {
	
	/**
	 * 公共短信内容,可为空,不为空则所有<message>节点内的<content>内容节点,在短信下发时都默认加上<publicContent>节点内容
	 */
	@XmlElement
	private String publicContent;
	
	@XmlElement
	private List<SmsMessage> message;
	
}
