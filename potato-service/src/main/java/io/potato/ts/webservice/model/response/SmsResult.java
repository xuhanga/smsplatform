/**
 * 
 */
package io.potato.ts.webservice.model.response;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import io.potato.core.util.xml.DateTimeAdapter;
import lombok.Getter;
import lombok.Setter;

/**
 * 短信状态
 * @author timl
 * created: 2019年1月14日 下午3:20:10
 */
@XmlRootElement(name = "smsresult")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class SmsResult {
	
	/**
	 * 短信标识(插入下发短信返回的msgid)
	 */
	@XmlElement
	private String msgid;
	
	/**
	 * 0是成功，其他失败
	 */
	@XmlElement
	private Integer status;
	
	/**
	 * 网关状态,0成功,其他失败
	 */
	@XmlElement
	private Integer msgstatus;
	
	/**
	 * 结果说明
	 */
	@XmlElement
	private String resultmsg;
	
	/**
	 * 手机获取短信时间
	 */
	@XmlElement
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private LocalDateTime senttime;
	
	/**
	 * 预留
	 */
	@XmlElement
	private String reserve = "";

	public SmsResult(String msgid, Integer status, Integer msgstatus, String resultmsg, LocalDateTime senttime) {
		super();
		this.msgid = msgid;
		this.status = status;
		this.msgstatus = msgstatus;
		this.resultmsg = resultmsg;
		this.senttime = senttime;
	}
	
	public SmsResult() {
		
	}
	
}
