/**
 * 
 */
package io.potato.ts.webservice;

import org.springframework.stereotype.Component;

/**
 * 短信对接接口服务
 * @author timl
 * created: 2019年1月14日 下午5:38:10
 */
@Component
public interface SmsApiService {
	
	/**
	 *  验证用户是否合法
	 * @param username 用户名
	 * @param password 密码
	 * @return  XML结果字符
	 */
	String checkUser(String username,  String password);
	
	/**
	 * 批量获取上发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param destaddr 
	    * 用户附加的扩展码， 不需要系统分配的三位扩展码
	 * @return XML结果字符
	 */
	String getUpSms(String username, String password,  String destaddr);
	
	/**
	 * 获取上行短信确认
	 * @param username 用户名
	 * @param password 密码
	 * @param msgid 短信唯一标识(批量获取上发短信返回的msgid),多个唯一表示用英文逗号(,)隔开,例如: 122,123,124,125
	 * @return XML结果字符
	 */
	String rspUpSms( String username,  String password, String msgid);
	
	/**
	 * 插入下发短信
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 接入号的扩展码(不超过两位),各个系统用同一账号情况下请用不同批次号码
	 * @param sendbody 发送主体
	 * @return XML结果字符
	 */
	String insertDownSms( String username,  String password, String batch,  String sendbody);
	
	/**
	 * 下发短信状态报告
	 * @param username 用户名
	 * @param password 密码
	 * @param Batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param cnt 获取的短信状态条数 <=10
	 * @return XML结果字符
	 */
	String getDownSmsResult( String username, String password, String Batch, String cnt);
	
	/**
	 * 获取指定下发短信状态
	 * @param username 用户名
	 * @param password 密码
	 * @param batch 批次,各个系统用同一账号情况下请用不同批次号码
	 * @param msgid 短信标识,多个标识可用英文逗号(,)隔开,例如:1000,1002,1003
	 * @return
	 */
	String getSpecialDownSmsResult( String username, String password, String batch, String msgid);
	
}
