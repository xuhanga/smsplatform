/**
 * 
 */
package io.potato.ts.webservice.model.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 短信状态
 * @author timl
 * created: 2019年1月14日 下午3:25:14
 */
@XmlRootElement(name = "response")
public class SmsResultResponse extends Response {
	
	private Body body;
	
	public SmsResultResponse(Integer smsresultcnt, List<SmsResult> smsresult) {
		this.body = new Body(smsresultcnt, smsresult);
	}
	
	public SmsResultResponse(List<SmsResult> smsresult) {
		this.body = new Body(smsresult);
	}
	
	public SmsResultResponse() {
		
	}
	
	public SmsResultResponse(Integer code, String message) {
		super(code, message);
	}
	
	@XmlElement
	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	@XmlRootElement(name = "body")
	static class Body {
		private Integer smsresultcnt = null;
		private List<SmsResult> smsresult;
		
		public Body(Integer smsresultcnt, List<SmsResult> smsresult) {
			this.smsresultcnt = smsresultcnt;
			this.smsresult = smsresult;
		}
		
		public Body(List<SmsResult> smsresult) {
			this.smsresult = smsresult;
		}
		
		public Body() {
			
		}
		
		@XmlElement
		public Integer getSmsresultcnt() {
			return smsresultcnt;
		}
		
		public void setSmsresultcnt(Integer smsresultcnt) {
			this.smsresultcnt = smsresultcnt;
		}
		
		@XmlElement
		public List<SmsResult> getSmsresult() {
			return smsresult;
		}
		
		public void setSmsresult(List<SmsResult> smsresult) {
			this.smsresult = smsresult;
		}
		
		
	}

}
