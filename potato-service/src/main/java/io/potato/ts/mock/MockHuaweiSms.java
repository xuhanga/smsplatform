/**
 * 
 */
package io.potato.ts.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.potato.core.util.Strings;
import io.potato.ts.sms.SmsRequest;
import io.potato.ts.sms.SmsResponse;
import io.potato.ts.sms.SmsResponse.SmsID;
import lombok.extern.slf4j.Slf4j;

/**
 * @author timl
 * created: 2019年2月28日 下午4:01:29
 */
@Component
@Slf4j
public class MockHuaweiSms {
	
	@Autowired
	RestTemplate restTemplate;
	
	ExecutorService poolExecutor = Executors.newFixedThreadPool(200);
	
	static AtomicInteger count = new AtomicInteger(0);
	
	public SmsResponse batchSendDiffSms(SmsRequest smsRequest) {
		List<SmsID> smsList = new ArrayList<>();
		smsRequest.getSmsContent().forEach(sc -> {
			for (String mobile : sc.getTo()) {
				SmsID smsID = new SmsID();
				smsID.setOriginTo(mobile);
				smsID.setSmsMsgId(Strings.uuid());
				smsID.setStatus("000000");
				smsList.add(smsID);
			}
		});
		
		poolExecutor.submit(() -> {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for (SmsID sms : smsList) {
				if (count.incrementAndGet() % 100 == 0) {
					log.info("SEND " + count.intValue());
				}
				//log.info("send " + sms.getSmsMsgId() + "   " + sms.getOriginTo());
				String url = smsRequest.getStatusCallback();
				MultiValueMap<String, String> param = new LinkedMultiValueMap<>();
				param.add("smsMsgId", sms.getSmsMsgId());
				param.add("total", "1");
				param.add("sequence", "1");
				param.add("status", randomStatus());
				param.add("orgCode", "0000000");
				param.add("source", "1");
				param.add("to", sms.getOriginTo());
				param.add("extend", smsRequest.getExtend());
				//post(smsRequest.getStatusCallback(), param);
				
				try {
					this.restTemplate.postForObject(url, param, String.class);
				} catch (Throwable e) {
					log.error(e.getMessage());
				}
			}
		});
		
		SmsResponse res = new SmsResponse();
		res.setCode("000000");
		res.setDescription("Success");
		res.setResult(smsList);
		return res;
	}
	
	public String randomStatus() {
		Random r = new Random();
		int a = r.nextInt(9);
		if (a == 5) {
			return "REJECTD";
		} else {
			return "DELIVRD";
		}
	}
	
	/**
	 * post 请求， 返回Map结果， 返回格式必须是JSON
	 * @param url  请求URL
	 * @param param  请求参数
	 * @return JSON转为Map返回
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<Map> post(String url, Map<String, Object> param) {
		ResponseEntity<Map> result = restTemplate.postForEntity(url, new HttpEntity<Object>(param, null), Map.class);
		return result;
	}
	
}
