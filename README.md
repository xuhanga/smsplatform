# smsplatform

#### 介绍
短信平台，支持大批量，高并发发送
发送程序都可以水平扩展，部署多个扩展发送能力

#### 软件架构
Spring Boot 2.0 + mysql + redis
JAVA 8
接口同时支持http rest 和webservice


#### 安装教程

1. 执行脚本 创建数据库
2. build.cmd构建

### 谁在使用？
深圳政府短信平台
地址：https://szsms.sz.gov.cn:8000

### 为什么开源？
收不到项目款
